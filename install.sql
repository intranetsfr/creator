-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           10.4.8-MariaDB - mariadb.org binary distribution
-- SE du serveur:                Win64
-- HeidiSQL Version:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Listage de la structure de la table intranets. pages
CREATE TABLE IF NOT EXISTS `pages` (
  `pages_id` int(11) NOT NULL AUTO_INCREMENT,
  `pages_parent` int(11) NOT NULL,
  `pages_index` int(11) NOT NULL,
  `pages_path` varchar(255) NOT NULL,
  `pages_class` varchar(255) NOT NULL,
  `pages_function` varchar(255) NOT NULL,
  `pages_value` text NOT NULL,
  `pages_template` enum('true','false') NOT NULL DEFAULT 'false',
  `pages_label` varchar(255) NOT NULL,
  `pages_show_main` enum('true','false') NOT NULL DEFAULT 'false',
  `pages_redirection` varchar(255) NOT NULL,
  PRIMARY KEY (`pages_id`),
  KEY `pages_index` (`pages_index`)
) ENGINE=InnoDB AUTO_INCREMENT=896 DEFAULT CHARSET=utf8;

