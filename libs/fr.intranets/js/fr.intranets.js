var Intranets = {};
Intranets.query = function (url, data, callback) {
	$(function () {
		$.post(base_url + url, data)
			.done(function (data) {
				callback(data);
			});
	});
};

Intranets.PageDelete = function (pages_path) {
	if (confirm("Voulez-vous vraiment supprimer cette page ?")) {
		Intranets.query("admin/dashboard", {pages_path: pages_path, action: "delete"}, function (result) {
			console.log(result);
			if (result == "true") {
				window.location.href = base_url + "admin/dashboard"
			}
		});
	}
};

Intranets.ReloadPageParent = function () {
	try {
		window.parent.reloadTree();
	} catch (error) {

	}
};
Intranets.propretyClose = function (pages_id) {
	window.parent.Editor.showProps(false);
};
