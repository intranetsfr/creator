<?php

class Intranet extends CI_Model
{
	private $components_list = array();


	public function __construct()
	{
		$this->dirToArray('application/models/components');

	}

	public function getParent($pages_id)
	{
		$this->db->select("pages.pages_parent, pages.pages_id");
		$this->db->where("pages.pages_id", $pages_id);
		$result = $this->db->get("pages")->result("array")[0];
		if ($result) {
			return $result;
		}
	}

	public function dirToArray($dir)
	{

		error_reporting(0);
		$result = array();

		$cdir = scandir($dir);
		foreach ($cdir as $key => $value) {
			if (!in_array($value, array(".", ".."))) {
				if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) {
					$result[$value] = $this->dirToArray($dir . '/' . $value);
				} else {
					$info = pathinfo($dir . '/' . $value);
					$info['dirname'] = str_replace('application/models/', '', $info['dirname']) . '/';

					$this->load->model($info['dirname'] . $info['filename'], $info['filename']);
					$class_methods = @get_class_methods($info['filename']);
					foreach ($class_methods as $method) {
						$r = new ReflectionMethod($info['filename'], $method);
						$rparams = $r->getParameters();
						foreach ($rparams as $param) {
							if ($param->isOptional()) {
								$info['functions'][$method][$param->getName()] = $param->getDefaultValue();
							}
						}
					}

					$result[$value] = $this->components_list[$info['filename']] = $info;
				}
			}
		}
		return $result;
	}

	public function get_diff($old, $new)
	{
		$from_start = strspn($old ^ $new, "\0");
		$from_end = strspn(strrev($old) ^ strrev($new), "\0");

		$old_end = strlen($old) - $from_end;
		$new_end = strlen($new) - $from_end;

		$start = substr($new, 0, $from_start);
		$end = substr($new, $new_end);
		$new_diff = substr($new, $from_start, $new_end - $from_start);
		$old_diff = substr($old, $from_start, $old_end - $from_start);

		$new = "$start<ins style='background-color:#ccffcc'>$new_diff</ins>$end";
		$old = "$start<del style='background-color:#ffcccc'>$old_diff</del>$end";
		return array("old" => $old, "new" => $new, "old_diff" => $old_diff, "newDiff" => $new_diff);
	}

	function diff($old, $new)
	{
		$matrix = array();
		$maxlen = 0;
		foreach ($old as $oindex => $ovalue) {
			$nkeys = array_keys($new, $ovalue);
			foreach ($nkeys as $nindex) {
				$matrix[$oindex][$nindex] = isset($matrix[$oindex - 1][$nindex - 1]) ?
					$matrix[$oindex - 1][$nindex - 1] + 1 : 1;
				if ($matrix[$oindex][$nindex] > $maxlen) {
					$maxlen = $matrix[$oindex][$nindex];
					$omax = $oindex + 1 - $maxlen;
					$nmax = $nindex + 1 - $maxlen;
				}
			}
		}
		if ($maxlen == 0) return array(array('d' => $old, 'i' => $new));
		return array_merge(
			diff(array_slice($old, 0, $omax), array_slice($new, 0, $nmax)),
			array_slice($new, $nmax, $maxlen),
			diff(array_slice($old, $omax + $maxlen), array_slice($new, $nmax + $maxlen)));
	}

	function htmlDiff($old, $new)
	{
		$ret = '';
		$diff = diff(preg_split("/[\s]+/", $old), preg_split("/[\s]+/", $new));
		foreach ($diff as $k) {
			if (is_array($k))
				$ret .= (!empty($k['d']) ? "<del>" . implode(' ', $k['d']) . "</del> " : '') .
					(!empty($k['i']) ? "<ins>" . implode(' ', $k['i']) . "</ins> " : '');
			else $ret .= $k . ' ';
		}
		return $ret;
	}

	public function getComponents($component_name = "")
	{
		return (!empty($component_name)) ? $this->components_list[$component_name] : $this->components_list;
	}

	public function afficher_menu($parent, $niveau, $array, $allow_debug = false)
	{
		$html = "";
		$niveau_precedent = 0;
		foreach ($array as $noeud) {
			if ($parent == $noeud['pages_parent']) {
				$data['noeud'] = $noeud;
				$noeud['values'] = (array)json_decode($noeud['pages_value'])[0];
				$data['propreties'] = $this->components_list[$noeud["pages_class"]]['functions'][$noeud["pages_function"]];

				$noeud['values'] = array_replace($data['propreties'], (array)$noeud['values']);


				$data['propreties']['children'] = '';
				$class = @new $noeud['pages_class'];
				if ($class) {
					$noeud['allow_debug'] = $allow_debug;
					$noeud['values']['children'] = $this->afficher_menu($noeud['pages_id'], ($niveau + 1), $array, $allow_debug);
					if ($allow_debug == true && $noeud['pages_parent'] !== "0") {
						//$html .= '<span class="debug" id="element_' . $noeud['pages_id'] . '"><a name="link_' . $noeud['pages_id'] . '" style="visibility: hidden;width: 0px !important;"></a>';
					}

					//$class->setParent($noeud);
					$html .= @call_user_func_array(array($class, $noeud['pages_function']), $noeud['values']);
					if ($allow_debug == true && $noeud['pages_parent'] !== 0) {
					//	$html .= '</span>';
					}
				} else {
					$html .= 'no class : ' . $class;
				}

			}
		}

		return $html;
	}

	public function afficher_menu_id($parent, $niveau, $array, $widthChildren)
	{

		$html = "";
		$niveau_precedent = 0;

		foreach ($array as $noeud) {
			if ($parent == $noeud['pages_id']) {
				$data['noeud'] = $noeud;
				$noeud['values'] = (array)json_decode($noeud['pages_value'])[0];
				$data['propreties'] = $this->components_list[$noeud["pages_class"]]['functions'][$noeud["pages_function"]];

				$noeud['values'] = array_replace($data['propreties'], (array)$noeud['values']);


				$data['propreties']['children'] = '';
				$class = new $noeud['pages_class'];
				if ($class) {
					if ($widthChildren) {
						$noeud['values']['children'] = $this->afficher_menu($noeud['pages_id'], ($niveau + 1), $array);
					} else {
						$noeud['values']['children'] = "";
					}
					$html .= @call_user_func_array(array($class, $noeud['pages_function']), $noeud['values']);
				} else {
					$html .= 'no class : ' . $class;
				}

			}
		}

		return $html;
	}
}
