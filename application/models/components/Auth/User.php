<?php

class User extends MY_Model
{
	public function __construct()
	{

	}

	public function auth_user_password($labelEmail = "E-mail", $labelPassword = "Password", $labelButton = "Ok", $button_align = array("left", "right", "center"))
	{
		$html = '<form action="" method="POST" autocomplete="off">
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input" type="email" id="users_email">
    <label class="mdl-textfield__label" for="users_email">' . $labelEmail . '</label>
  </div>
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input" type="password" id="users_password">
    <label class="mdl-textfield__label" for="users_password">' . $labelPassword . '</label>
  </div>
  <div align="' . $button_align . '">
	  <button type="submit" class="mdl-button">' . $labelButton . '</button>
  </div>
</form>';
		return $html;
	}

	public function _is_connect($session_name)
	{
		if (null !== $this->session->userdata($session_name) and is_numeric($this->session->userdata($session_name))) {
			return $this->session->id;
		} else {
			return false;
		}
	}

	public function isConnected($children = "", $redirection_not_connected = "users/login.html", $debug = false)
	{
		$html = '';
		if ($this->_is_connect("u_u_token")) {
			$html .= $children;
		}
		if ($debug) {
			$html .= 'DEBGU';
		}
		return $html;
	}

	public function forgetPassword($text = "[LINK]")
	{
		return "FORMULAIRE FORGET Password";
	}

	public function facebook_auth($appID = "1d3ze1dz5e1dz", $token = "4dezd5ez")
	{
		return 'Facebook login !!!!';
	}
}
