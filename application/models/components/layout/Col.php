<?php

class Col extends MY_Model
{
	public function column(
		$children = "",
		$xs = array(
			"",
			"col-1",
			"col-2",
			"col-3",
			"col-4",
			"col-5",
			"col-6",
			"col-7",
			"col-8",
			"col-9",
			"col-10",
			"col-11",
			"col-12"
		),
		$sm = array(
			"",
			"col-sm-1",
			"col-sm-2",
			"col-sm-3",
			"col-sm-4",
			"col-sm-5",
			"col-sm-6",
			"col-sm-7",
			"col-sm-8",
			"col-sm-9",
			"col-sm-10",
			"col-sm-11",
			"col-sm-12"
		),
		$md = array(
			"",
			"col-md-1",
			"col-md-2",
			"col-md-3",
			"col-md-4",
			"col-md-5",
			"col-md-6",
			"col-md-7",
			"col-md-8",
			"col-md-9",
			"col-md-10",
			"col-md-11",
			"col-md-12"
		),
		$lg = array(
			"",
			"col-lg-1",
			"col-lg-2",
			"col-lg-3",
			"col-lg-4",
			"col-lg-5",
			"col-lg-6",
			"col-lg-7",
			"col-lg-8",
			"col-lg-9",
			"col-lg-10",
			"col-lg-11",
			"col-lg-12"
		),
		$offset_xs = array(
			"",
			"col-offset-1",
			"col-offset-2",
			"col-offset-3",
			"col-offset-4",
			"col-offset-5",
			"col-offset-6",
			"col-offset-7",
			"col-offset-8",
			"col-offset-9",
			"col-offset-10",
			"col-offset-11",
			"col-offset-12"
		),
		$offset_sm = array(
			"",
			"col-sm-offset-1",
			"col-sm-offset-2",
			"col-sm-offset-3",
			"col-sm-offset-4",
			"col-sm-offset-5",
			"col-sm-offset-6",
			"col-sm-offset-7",
			"col-sm-offset-8",
			"col-sm-offset-9",
			"col-sm-offset-10",
			"col-sm-offset-11",
			"col-sm-offset-12"
		),
		$offset_md = array(
			"",
			"col-md-offset-1",
			"col-md-offset-2",
			"col-md-offset-3",
			"col-md-offset-4",
			"col-md-offset-5",
			"col-md-offset-6",
			"col-md-offset-7",
			"col-md-offset-8",
			"col-md-offset-9",
			"col-md-offset-10",
			"col-md-offset-11",
			"col-md-offset-12"
		),
		$offset_lg = array(
			"",
			"col-lg-offset-1",
			"col-lg-offset-2",
			"col-lg-offset-3",
			"col-lg-offset-4",
			"col-lg-offset-5",
			"col-lg-offset-6",
			"col-lg-offset-7",
			"col-lg-offset-8",
			"col-lg-offset-9",
			"col-lg-offset-10",
			"col-lg-offset-11",
			"col-lg-offset-12"
		),
		$visible_xs = array(
			"",
			"visible-xs",
			"hidden-xs",

		),
		$visible_sm = array(
			"",
			"visible-sm",
			"hidden-sm",
		),
		$visible_md = array(
			"",
			"visible-md",
			"hidden-md",
		),
		$visible_lg = array(
			"",
			"visible-lg",
			"hidden-lg",
		),
		$align = array(
			"left",
			"center",
			"justify",
			"right"
		)
	)
	{
		return '<div class="' . $xs . ' ' . $sm . ' ' . $md . ' ' . $lg . ' ' . $offset_xs . ' ' . $offset_sm . ' ' . $offset_md . ' ' . $offset_lg .' ' . $visible_xs .' ' . $visible_sm .' ' . $visible_md .' ' . $visible_lg .'" align="' . $align . '">' . $children . '</div>';

	}
}
?>
