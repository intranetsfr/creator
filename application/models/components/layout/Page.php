<?php

class Page extends MY_Model
{
	public function page($title = "Page'title", $scripts = "", $meta_keys = "", $meta_description = "", $activeGoogleTagManager = false, $GoogleTagManagerID = "GTM-*****",
						 $indexation = array("noindex", "nofollow", "noarchive", "none"), $GoogleBot = '',
						 $ogImage = "file.upload", $children = "", $favicon = "favicon.upload"
	)
	{
		//GTM-NKT8DJ3
		$csss = array(
			site_url('libs/material-design-lite/material.min.css'),
			'https://fonts.googleapis.com/icon?family=Material+Icons',
			site_url("libs/bootstrap-grid/css/bootstrap.min.css"),
			site_url("libs/components-font-awesome/css/all.min.css"),
			site_url('libs/ipaf/css/ipaf.css?time='.time()),
		);
		$html = '<!DOCTYPE html>';
		$html .= '<html lang="fr" dir="ltr">';
		$html .= '<head>';
		$html .= '<title>' . $title . '</title>';
		$html .= '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
		$html .= '<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">';
		foreach ($csss as $css) {
			$html .= link_tag($css);
		}
		$html .= '<meta property="og:title" content="' . $title . '" /><meta property="og:type" content="website" />';
		if (!empty($meta_description)) {
			$html .= '<meta name="description" content="' . $meta_description . '"/>';
			$html .= '<meta property="og:description" content="' . $meta_description . '"/>';
		}
		if (!empty($meta_keys)) {
			$html .= '<meta name="keywords" content="' . $meta_keys . '"/>';
		}
		if (!empty($indexation)) {
			//$html .= '<meta name="robots" content="' . $indexation . '" />';
		}
		if (!empty($GoogleBot)) {
			//$html .= '<meta name="Googlebot" content="' . $GoogleBot . '" />';
		}
		if (!empty($ogImage)) {
			$html .= '<meta property="og:image" content="' . $ogImage . '" />';
		}
		$html .= '<meta name="generator" content="Intranets 2.0" />';
		$html .= '<script type="text/javascript">var base_url = "' . site_url() . '";</script>';
		if ($scripts !== 'block.scripts') {
			$html .= $scripts;
		}
		$html .= '<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({\'gtm.start\':
new Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';j.async=true;j.src=
\'https://www.googletagmanager.com/gtm.js?id=%27+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,\'script\',\'dataLayer\',\'GTM-P7X3S4P\');</script>
<!-- End Google Tag Manager -->';
		if ($activeGoogleTagManager == true) {
			$html .= '<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({\'gtm.start\':
new Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';j.async=true;j.src=
\'https://www.googletagmanager.com/gtm.js?id=\'+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,\'script\',\'dataLayer\',\'' . $GoogleTagManagerID . '\');</script>
<!-- End Google Tag Manager -->';
		}
		$html .= '</head>';
		$html .= '<body>';
		if ($activeGoogleTagManager) {
			$html .= '
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=' . $GoogleTagManagerID . '"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->';
		}
		$html .= $children;


		$scripts = array(
			site_url('libs/material-design-lite/material.min.js'),
			site_url('libs/fr.intranets/js/menu.js'),
		);
		foreach ($scripts as $script) {
			$html .= '<script type="text/javascript" src="' . $script . '"></script>';
		}
		$html .= '</body>';
		$html .= '</html>';
		return $html;
	}

	public function header($children = "")
	{
		$html = '<header>';
		$html .= $children;
		$html .= '</header>';
		return $html;
	}

	public function footer($children = "", $script = "", $class="mdl-mini-footer")
	{
		$html = '<footer class="'.$class.'">
  <div class="mdl-mini-footer__left-section">
  ' . $children . '
  </div>
</footer>' . $script;
		return $html;
	}

	public function style($css = "contenu.text")
	{
		$html = '<style>';
		$html .= $css;
		$html .= '</style>';
		return trim($html);
	}

	public function template($template_name = "template_name.template", $withChildren = false)
	{
		$withChildren === "on" ? $withChildren = true : false;
		if (!empty($template_name) || $template_name !== 'template_name.template') {
			$array = $this->db->order_by("pages_index", "asc")->get("pages")->result("array");
			$info = $this->Intranet->getParent($template_name);
			$html = $this->Intranet->afficher_menu_id($info['pages_id'], 0, $array, $withChildren);
		} else {
			$html = 'SELECT A TEMPLATE';
		}
		return $html;
	}
}
