<?php

class Listing extends MY_Model
{
	public function order($order = "order.fields", $direction = array("asc", "desc"))
	{
		$this->db->order_by();
		$html = $order . '';
		$html .= "Ordonné par";
		return $html;
	}

	public function listing($table = "pages.sql", $children = "", $limit = 30, $per_page = 20, $pagination = array("", "top", "bottom", "top-bottom"), $order = "list.fields", $order_by = array("asc", "desc"))
	{
		$html = '';
		$table = str_replace(".sql", "", $table);
		//e($table, true);
		$total = $this->db->select('COUNT(*) as c')->get($table)->result("array")[0]['c'];
		$config['base_url'] = current_url();
		$config['total_rows'] = $total;

		$config['per_page'] = $per_page;
		$config['full_tag_open'] = '<div class="pagination col-xs-12" align="right" style="width: 100%;display: block;">';
		$config['full_tag_close'] = '</div>';
		$config['per_page'] = $limit;
		$config['num_links'] = 5;
		$config['page_query_string'] = TRUE;
		$config['prev_link'] = '&lt;';
		$config['prev_tag_open'] = '<span class="mdl-button">';
		$config['prev_tag_close'] = '</span>';
		$config['next_link'] = '&gt;';
		$config['next_tag_open'] ='<span class="mdl-button">';
		$config['next_tag_close'] = '</span>';
		$config['cur_tag_open'] = '<span class="active"><a href="#" class="mdl-button disabled">';
		$config['cur_tag_close'] = '</a></span>';
		$config['num_tag_open'] = '<span class="mdl-button">';
		$config['num_tag_close'] = '</span>';
		$config['first_link'] = FALSE;
		$config['last_link'] = FALSE;
		$this->pagination->initialize($config);
		if ($limit > 0) {
			if (isset($_GET['per_page']) && is_numeric($_GET['per_page']) && $_GET['per_page'] > 0) {
				$this->db->limit($limit, $_GET['per_page']);

			} else {
				$this->db->limit($limit, 0);
			}
		}
		if (!empty($order) && $order !== "list.fields" && !empty($order_by)) {
			$order = str_replace(array(
				'[',
				']'
			), array(
				'.',
				''
			), $order);
			$this->db->order_by($order, $order_by);
		}

		$items = $this->db->get($table);


		$result = $items->result("array");
		$fields = $this->db->list_fields($table);

		$this->pagination->initialize($config);
		if (!empty($pagination) && ($pagination == 'top' || $pagination == 'top-bottom')) {
			$html .= $this->pagination->create_links();
		}

		for ($i = 0; $i < count($result); $i++) {
			$search = array();
			$replace = array();

			foreach ($fields as $field) {
				array_push($search,
					"@" . $table . "." . $field
				);

				array_push($replace,
					$result[$i][$field]
				);
			}
			$html .= str_replace($search, $replace, $children);
		}

		if (!empty($pagination) && ($pagination == 'bottom' || $pagination == 'top-bottom')) {
			$html .= $this->pagination->create_links();;
		}
		return $html;
	}

	public function while_($children = "", $i = 3)
	{
		$html = '';
		for ($j = 0; $j < $i; $j++) {
			array_push($search,
				"@i"
			);

			array_push($replace,
				$j
			);
			$html .= str_replace($search, $replace, $children);
		}
		return $html;
	}

	public function ul($children = "", $type = array("", "a", "A", "i", "I", 1), $class = "mdl-menu mdl-menu--bottom-left mdl-js-menu mdl-js-ripple-effect", $for = "id")
	{
		$html = '<ul class="' . $class . '"';
		if (!empty($type) && gettype($type) !== 'array') {
			$html .= ' type="' . $type . '"';
		}
		$html .= ' for="' . $for . '">';
		$html .= $children;
		$html .= '</ul>';
		return $html;
	}

	public function dot($type = array("ol", "li"), $children = "", $value = 0, $class = array("mdl-menu__item", "mdl-menu__item mdl-menu__item--full-bleed-divider"))
	{
		$html = '<' . $type;
		if (!empty($value)) {
			$html .= ' value="' . $value . '"';
		}
		$html .= ' class="' . $class . '">' . $children;
		$html .= '</' . $type . '>';
		return $html;
	}
}
