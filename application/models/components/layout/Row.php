<?php

class Row extends MY_Model
{
	public function row($children = "")
	{

		$html = '<div class="row">';
		$html .= $children;
		$html .= '</div>';

		return $html;
	}

	public function block($align = array("center", "left", "right"),
						  $margin_top = "10px",
						  $margin_bottom = "10px",
						  $margin_left = "10px",
						  $margin_right = "10px",
						  $padding_top = "10px",
						  $padding_bottom = "10px",
						  $padding_left = "10px",
						  $padding_right = "10px",
						  $width = "100%",
						  $height = "100%",
						  $position = array("", "absolute", "fixed", "relative", "static"),
						  $top = "0px",
						  $bottom = "0px",
						  $left = "0px",
						  $right = "0px",
						  $z_index = "0",
						  $class = "",
						  $style = "",
						  $id = "",
						  $children = "")
	{
		$html = '<div ';
		if ($align !== "Array") {
			$html .= 'align="' . $align . '" ';
		}
		$html .= 'style="' . $style;
		if (!empty($margin_top)) {
			$html .= 'margin-top: ' . $margin_top . ';';
		}
		if (!empty($margin_left)) {
			$html .= 'margin-left: ' . $margin_left . ';';
		}
		if (!empty($margin_right)) {
			$html .= 'margin-right: ' . $margin_right . ';';
		}
		if (!empty($margin_bottom)) {
			$html .= 'margin-bottom: ' . $margin_bottom . ';';
		}
		if (!empty($padding_top)) {
			$html .= 'padding-top: ' . $padding_top . ';';
		}
		if (!empty($padding_left)) {
			$html .= 'padding-left: ' . $padding_left . ';';
		}
		if (!empty($padding_right)) {
			$html .= 'padding-right: ' . $padding_right . ';';
		}
		if (!empty($padding_bottom)) {
			$html .= 'padding-bottom: ' . $padding_bottom . ';';
		}
		if (!empty($width)) {
			$html .= 'width: ' . $width . ';';
		}
		if (!empty($height)) {
			$html .= 'height: ' . $height . ';';
		}
		if (!empty($position)) {
			$html .= 'position: ' . $position . ';';
			if (!empty($top)) {
				$html .= 'top: ' . $top . ';';
			}
			if (!empty($bottom)) {
				$html .= 'bottom: ' . $bottom . ';';
			}
			if (!empty($left)) {
				$html .= 'left: ' . $left . ';';
			}
			if (!empty($right)) {
				$html .= 'right: ' . $right . ';';
			}
			if (!empty($z_index)) {
				$html .= 'z-index: ' . $z_index . ';';
			}
		}

		$html .= '" class="' . $class . '" id="' . $id . '">' . $children . '</div>';
		return $html;
	}

	public function backgroundImage($image = "libs/fr.intranets/images/welcome_card.upload",
									$background_repeat = array("", "initial", "no-repeat", "repeat", "repeat-x", "repeat-y", "round"),
									$background_position = array("", "left", "right", "bottom", "top", "center", "fixed"),
									$background_size = "/*auto contain cover  center 100% 10%*/",
									$padding = "10px",
									$children = "")
	{
		$html = '<div style="';
		if (!empty($image)) {
			$html .= 'background-image: url(\'' . $image . '\');';
		}
		if (!empty($padding)) {
			$html .= 'padding: ' . $padding . ";";
		}
		if (!empty($background_repeat)) {
			$html .= 'background-repeat: ' . $background_repeat . ';';
		}
		if (!empty($background_position)) {
			$html .= 'background-position: ' . $background_position . ';';
		}
		if (!empty($background_size)) {
			$html .= 'background-size: ' . $background_size . ';';
		}
		$html .= '">' . $children . '</div>';
		return $html;
	}

	public function background_color($red = 0,
									 $blue = 0,
									 $green = 0,
									 $alpha = '0',
									 $children = "")
	{
		$html = '<div style="background-color: rgba(' . $red . ', ' . $blue . ', ' . $green . ', ' . $alpha . ')';
		$html .= '">' . $children . '</div>';
		return $html;
	}

	public function border($children = "", $size = 1, $red = 0, $green = 0, $blue = 0, $alpha = "", $type = array("solid", "dotted", "double", "inset", "ouset"), $radius = 0)
	{
		$html = '<div style="border-radius: ' . $radius . 'px;border: ' . $type . ' ' . $size . 'px rgba(' . $red . ', ' . $green . ', ' . $blue . ', ' . $alpha . ')">' . $children . '</div>';
		return $html;
	}

	public function backgroundVideo($image = "image.upload", $background = array("no-repeat", "repeat"))
	{

	}
}

?>
