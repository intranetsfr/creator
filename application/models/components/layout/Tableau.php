<?php

class Tableau extends MY_Model
{
	public function table($children = "", $width = "100%", $border = array(0, 1))
	{
		$html = '<table';
		if (!empty($width)) {
			$html .= ' width="100%"';
		}
		if ($border == '1') {
			$html .= ' border="' . $border . '" ';
		}
		$html .= '>';
		$html .= $children . '</table>';
		return $html;
	}

	public function tfoot($children = "")
	{
		$html = '<tfoot>' . $children . '</tfoot>';
		return $html;
	}

	public function tr($children = "", $width = "100%", $align = array("", "left", "right", "center", "justify", "char"))
	{

		return '<tr  width="' . $width . '" style="text-align: ' . $align . '">' . $children . '</tr>';
	}

	public function th($children = "", $width = "", $align = array("", "left", "right", "center", "justify", "char"))
	{

		return '<th align="' . $align . '" ' . (!empty('width="' . $width . '"') ? $width : '') . '>' . $children . '</th>';
	}

	public function td($children = "", $width = "100%", $align = array("", "left", "right", "center", "justify", "char"))
	{

		return '<td width="' . $width . '" style="text-align: ' . $align . '">' . $children . '</td>';
	}

	public function tbody($children = "")
	{
		return '<tbody>' . $children . '</tbody>';
	}

	public function thead($children = "")
	{
		return '<thead>' . $children . '</thead>';
	}
}
