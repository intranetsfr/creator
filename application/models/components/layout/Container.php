<?php

class Container extends MY_Model
{
	public function container($children = "", $size = array("container", "container-fuild"))
	{
		$html = '<div class="' . $size . '">';
		$html .= $children;
		$html .= '</div>';
		return $html;
	}
}
