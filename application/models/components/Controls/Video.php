<?php

class Video extends MY_Model
{
	public function youtube($url = "https://www.youtube.com/watch?v=iFZw5lrVvjY", $autoplay = false, $width = "961px", $height = "721px")
	{
		/*
		 * <iframe width="1280" height="720" src="https://www.youtube.com/embed/vhw_S-GCxI8?list=PLVt1NA5Do9U1WGgBwm0y1YNV6-ivqs5IH" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		 */
		$url = 'https://www.youtube.com/embed/' . $url;
		$html = '<iframe width="' . $width . '" height="' . $height . '" src="' . $url . '" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
		return $html;
	}

	public function video_simple($file = "http://www.papytane.com/mp4/arnaudin.mp4", $autoplay = true, $controls = true, $width = "400px", $height = "400px")
	{
		$html = '<video ';
		e($autoplay);
		if ($autoplay == "on") {
			$html .= 'autoplay ';
		}
		$html .= 'src="' . $file . '" ';
		if ($controls == "on") {
			$html .= 'controls ';
		}
		$html .= 'width="' . $width . '" ';
		$html .= 'height="' . $height . '" ';
		$html .= '>';
		$html .= '</video>';
		return $html;
	}
}
