<?php

class Share extends MY_Model
{
	public function fb_root($lang = "en_US")
	{
		$html = '<div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/' . $lang . '/sdk.js#xfbml=1&version=v3.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, \'script\', \'facebook-jssdk\'));</script>';
		return $html;
	}

	public function facebook($button_type = array("button_count", "box_count", "button"))
	{
		return '<div class="fb-share-button" 
    data-href="' . current_url() . '" 
    data-layout="' . $button_type . '">
  </div>';
	}

	public function twitter($text = "", $children = "")
	{
		return '<a href="https://twitter.com/intent/tweet?text='.$text.'" data-size="large" target="_blank">' . $children . '</a>';
	}
}
