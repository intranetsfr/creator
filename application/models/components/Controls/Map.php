<?php

class Map extends MY_Model
{
	public function google_map($api = "789", $lat = "43", $lng = "2", $zoom = 4, $width = "100%", $heigt = "300px")
	{
		$t = time();
		$html = '<div id="map_' . $t . '" style="width: ' . $width . ';height: ' . $heigt . '"></div>
    <script>
// Initialize and add the map
function initMap' . $t . '() {
  var uluru = {lat: ' . $lat . ', lng: ' . $lng . '};
  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById(\'map_' . $t . '\'), {zoom: ' . $zoom . ', center: uluru});
  // The marker, positioned at Uluru
  var marker = new google.maps.Marker({position: uluru, map: map});
}
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=' . $api . '&callback=initMap' . $t . '">
    </script > ';

		return $html;

	}

	public function marker()
	{

	}
}
