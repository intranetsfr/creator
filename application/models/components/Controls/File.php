<?php

class File extends MY_Model
{
	public function __construct()
	{
		if (!isset($_POST['submit']) && isset($_POST['update']) && gettype($_POST['update']) !== 'array') {
			$count = count($_FILES);
			foreach ($_FILES as $key => $file) {

				$pages_path = $this->input->post("pages_path");
				$pages_id = $this->input->post("pages_id");
				$config['upload_path'] = './uploads/' . $pages_path . '/' . $pages_id . '/';
				@mkdir($config['upload_path'], 0777, true);

				$akamai = str_replace('./', '', site_url($config['upload_path']));
				$config['allowed_types'] = 'png|jpeg|jpg|gif|pdf|obj';
				$config['allowed_types'] = '*';
				$config['max_size'] = 409600;

				$config['max_size'] = '3000000'; // Added Max Size
				$config['overwrite'] = TRUE;
				$config['max_width'] = 0;
				$config['max_height'] = 0;

				$_FILES['file']['name'] = $_FILES[$key]['name'];
				$_FILES['file']['type'] = $_FILES[$key]['type'];
				$_FILES['file']['tmp_name'] = $_FILES[$key]['tmp_name'];
				$_FILES['file']['error'] = $_FILES[$key]['error'];
				$_FILES['file']['size'] = $_FILES[$key]['size'];
				if ($_FILES['file']['size'] !== 0) {
					$this->load->library('upload');

					$this->upload->initialize($config);
					if ($this->upload->do_upload('file')) {
						$uploadData = $this->upload->data();
						$filename = $uploadData['file_name'];

						$data = $this->upload->data();
						$_POST["pages_value"][$key] = $akamai . $data['file_name'];
						$_POST[$key] = $akamai . $data['file_name'];
					} else {

						var_dump($this->upload->display_errors('', ''));
						e("", true);
					}
				}

			}
		}
	}

	public function image($src = 'file.upload', $alt = "Image title", $width = "100%", $height = "", $class = "", $padding = "0px")
	{
		$style = '';
		if ($src == 'file.upload' || empty($src)) {
			$src = 'https://via.placeholder.com/600x300';
		}
		if (!empty($padding)) {
			$style = 'style="';
			$style .= 'padding: ' . $padding . ";";
			$style .= '"';
		}
		return '<img src="' . $src . '" ' . $style . ' width="' . $width . '" alt="' . $alt . '" height="' . $height . '" class="' . $class . '" />';

	}

	public function background_image($src = 'file.upload', $children = "", $padding = "10px")
	{
		if ($src == 'file.upload') {
			$src = 'https://via.placeholder.com/600x300';
		}

		$html = '<div style="';
		if (!empty($src)) {
			$html .= 'background-image: url(\'' . $src . '\');';
		}
		$html .= '">' . $children . '</div>';
		return $html;
	}

	public function background_text($src = 'file.upload', $default_color = "white", $children = "", $padding = "10px")
	{
		if ($src == 'file.upload') {
			$src = 'https://via.placeholder.com/600x300';
		}
		$html = '<div style="';
		if (!empty($src)) {
			$html .= 'color: ' . $default_color . ';background-image: url(\'' . $src . '\');';
		}
		$html .= '-webkit-background-clip: text;-webkit-text-fill-color: transparent;">' . $children . '</div>';
		return $html;
	}

	public
	function download($src = "file.upload", $debug = true, $name = "Nom_du_fichier_avec_extension")
	{
		$html = '';

		$this->load->helper('download');
		if (!empty($src) && $src !== "file.upload") {
			if ($debug !== "true") {
				if (isset($_GET['download'])) {
					echo str_replace(site_url(), '', $src);
					//force_download('./' . str_replace(site_url(), '', $src), NULL);
					exit();
				} else {
					$tinyurl = file_get_contents('http://tinyurl.com/api-create.php?url=' . $src);
					$html .= '<a href="' . $tinyurl . '" class="mdl-button mdl-button-raised">' . $tinyurl . '</a>';

					$html .= '<iframe id="my_iframe" style="display:none;"></iframe>
<script>
function Download() {
    document.getElementById("my_iframe").src = "' . current_url() . '?download=true";
};
Download();
</script>';
					//redirect(current_url() . "?download=true");

				}
			} else {
				$html .= '<div style="border: solid 1px red;background: black;color: white;padding: 20px;text-align: center">Telechargement du Fichier</div>';
			}
		} else {
			$html .= '<div style="border: solid 1px red;background: black;color: white;padding: 20px;text-align: center">Telechargement du Fichier</div>';
		}
		return $html;
	}

	public function pdf($file = "file.upload", $height = "550")
	{
		$html = '<embed src="' . $file . '" width="100%" height="' . $height . '" type="application/pdf"> ';
		return $html;
	}
}

?>
