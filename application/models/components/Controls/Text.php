<?php

class Text extends MY_Model
{
	public function text($text = "My Text")
	{
		return $text;
	}

	public function heading($size = array(1, 2, 3, 4, 5, 6), $font_size = "11px", $title = "Title", $class = "default class", $alt = "Title'alt",
							$weight = array(
								"normal",
								"bold",
								"bolder",
								"lighter",
								"100",
								"200",
								"300",
								"400",
								"500",
								"600",
								"700",
								"800",
								"900",
								"inherit",
								"initial",
								"unset",
							),
							$line_height = "24px",
							$align = array("center", "left", "right", "justify"),
							$children = ""
	)
	{
		$html = '<h' . $size;
		if ($class) {
			$html .= ' class="' . $class . '"';
		}
		if ($alt) {
			$html .= ' alt="' . strip_tags(str_replace('"', "'", $alt) ). '"';
		}
		$html .= ' align="' . $align . '"';
		if (!empty($font_size) || !empty($weight)) {
			$html .= 'style="';
			if (!empty($font_size)) {
				$html .= 'font-size: ' . $font_size . ';';

			}
			if (!empty($weight)) {
				$html .= 'font-weight: ' . $weight . ';';
			}
			if (!empty($line_height)) {
				$html .= 'line-height: ' . $line_height . ';';
			}
			$html .= '"';
		}
		$html .= '>' . $title . $children . '</h' . $size . '>';
		return $html;
	}

	public function paragraph($text = 'Text.text',
							  $align = array("center", "right", "justify", "left",),
							  $padding = '10px',
							  $font_size = "",
							  $weight = array(
								  "normal",
								  "bold",
								  "bolder",
								  "lighter",
								  "100",
								  "200",
								  "300",
								  "400",
								  "500",
								  "600",
								  "700",
								  "800",
								  "900",
								  "inherit",
								  "initial",
								  "unset",
							  ),
							  $line_height = "24px"
	)
	{
		$html = '<p align="' . $align . '" ';
		if (!empty($font_size) || !empty($weight) || !empty($padding) || !empty($line_height)) {
			$html .= 'style="';
			if (!empty($padding)) {
				$html .= 'padding: ' . $padding . ';';
			}
			if (!empty($font_size)) {
				$html .= 'font-size: ' . $font_size . ';';

			}
			if (!empty($weight)) {
				$html .= 'font-weight: ' . $weight . ';';
			}
			if (!empty($line_height)) {
				$html .= 'line-height: ' . $line_height . ';';
			}
			$html .= '">';

		}
		if ($bold === true) {
			$html .= '<strong>';
		}
		$html .= nl2br($text, false);
		if ($bold === true) {
			$html .= '</strong>';
		}
		$html .= '</p>';
		return $html;
	}

	public function tag($text = "Text", $deletable = true)
	{
		$html = '<!-- Deletable Contact Chip -->
<span class="mdl-chip mdl-chip--contact mdl-chip--deletable">
    <span class="mdl-chip__text">' . $text . '</span>';
		if ($deletable === false) {
			$html .= '<a href="#" class="mdl-chip__action"><i class="material-icons">cancel</i></a>';
		}
		$html .= '</span>';
		return $html;
	}


	public
	function hr($class = "")
	{
		$html = '<hr ';
		if (!empty($class)) {
			$html .= 'class=""';
		}
		$html .= '/>';
		return $html;

	}

	public function code($text = "")
	{
		$html = '<pre>' . $text . '</pre>';
		return $html;
	}

}
