<?php

class Card extends MY_Model
{
	public function default_card($children = "", $height="300px")
	{
		$html = '<div class="mdl-card mdl-shadow--2dp" style="width:100%;'.(!empty($height)? $height:'').'">';

		$html .= '<div class="mdl-card__supporting-text">' . $children . '</div></div>';
		return $html;
	}

	public function Wide($title = "Title", $image = 'background.upload', $text = "Lorem Ipsum", $textAlign = array("center", "left", "right", "justify"))
	{
		$html = '<style>';
		$html .= '.demo-card-wide.mdl-card {
  width: 100%;
}
.demo-card-wide > .mdl-card__title {
  color: #fff;
  height: 176px;
  background: url("' . $image . '") center / cover;
}
.demo-card-wide > .mdl-card__menu {
  color: #fff;
}
</style>';
		$html .= '<div class="demo-card-wide mdl-card mdl-shadow--2dp"><div class="mdl-card__title"><h2 class="mdl-card__title-text">' . $title . '</h2></div>';
		$html .= '<div class="mdl-card__supporting-text" align="' . $textAlign . '">' . $text . '</div>';
		$html .= '<div class="mdl-card__actions mdl-card--border">';
		$html .= '</div>';
		$html .= '</div>';
		return $html;
	}

	public function image($image = 'background.upload', $children = "", $width = "100%", $height = "100%")
	{
		$html = '<!-- Image card -->
<style>
.demo-card-image.mdl-card {
  background: url("' . $image . '") center / cover;
}
.demo-card-image > .mdl-card__actions {
  height: 52px;
  padding: 16px;
  background: rgba(0, 0, 0, 0.2);
}
.demo-card-image__filename {
  color: #fff;
  font-size: 14px;
  font-weight: 500;
}
</style>

<div class="demo-card-image mdl-card mdl-shadow--2dp" style="width: ' . $width . ';height: ' . $height . '">
  <div class="mdl-card__title mdl-card--expand"></div>
  <div class="mdl-card__actions">
    <span class="demo-card-image__filename">' . $children . '</span>
  </div>
</div>
';
		return $html;
	}
}
