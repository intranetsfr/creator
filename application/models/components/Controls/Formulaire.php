<?php

class Formulaire extends MY_Model
{

	public function __construct()
	{
		$this->load->library('form_validation');
	}

	public function form($children = "", $autocomplete = array("on", "false"), $save_into = "saveinto.sql", $send_to_email = "", $action = array("insert", "update", "delete"), $redirection = "")
	{
		$html = '<form method="post" novalidate autocomplete="' . $autocomplete . '">';
		$html .= $children;
		if (isset($_POST) && !empty($_POST)) {
			//if ($this->form_validation->run() == FALSE) {
			//} else {
			$tables = $_POST;
			$data_insert = array();
			foreach ($tables as $table => $post) {
				$data_insert = array();
				$message = '';
				foreach ($post as $field_name => $value) {
					$data_insert[$field_name] = $value;
					$message .= $field_name . " => " . $value . "<br />";
				}
				$data_insert[$table . "_date_insert"] = date("Y-m-d H:i:s");
				$data_insert[$table . "_ip"] = $_SERVER['REMOTE_ADDR'];


				$message .= "Date d'envoie =>" . $data_insert[$table . "_date_insert"];

//contact@ipaf-paris.fr;contact.ipaf@gmail.com

				$headers[] = 'To: Website <' . $send_to_email . '>';
				$headers[] = 'From: Website <no-reply@' . str_replace(array("https://", "/"), "", site_url()) . '>';
				$headers[] = 'Cc: naceri.medi@gmail.com';

				$headers[] = 'Bcc: contact.ipaf@gmail.com;';
				$headers[] = 'MIME-Version: 1.0';
				$headers[] = 'Content-type: text/html; charset=utf-8';


				$mail = mail($send_to_email, "Nouveau message du site", $message, implode("\r\n", $headers));
				$this->db->$action($table, $data_insert);
				if ($mail) {
					redirect($redirection);
				} else {
					redirect($redirection);
				}

			}
		}
		$html .= validation_errors();

		$html .= '</form>';
		return $html;
	}

	public function input($type = array("text", "email", "password", "number", "date", "checkbox", "radio"), $name = "champs.fields", $autofocus = false, $is_unique = "data.sql", $pattern = "", $label = "Champs", $required = false, $min_length = "", $max_length = "", $enableJS = false, $error = "La valeur est incorrecte")
	{
		$id = rand(0, time());
		if ($enableJS !== false) {
			$enableJS = "mdl-js-textfield mdl-textfield--floating-label";
		} else {
			$enableJS = '';
		}
		if ($autofocus !== false) {
		} else {
			$autofocus = 'autofocus';
		}

		/*
		 * validations
		 */
		$validations = "trim|";
		$validation_array = array("trim" => "");
		if ($required == "on") {
			$required = 'required="required"';
			$validations .= 'required|';
			$validation_array["required"] = "La valeur du champs %s est obligatoire";
			$this->form_validation->set_message($name, $validation_array["required"]);
		} else {
		}
		if ($type !== 'email') {
			if (!empty($min_length)) {
				$validations .= 'min_length[' . $min_length . ']|';
				$validation_array["min_length"] = "La valeur du champs	  %s est trop courte";
				$this->form_validation->set_message($name, $validation_array["min_length"]);
			}
			if (!empty($max_length)) {
				$validations .= 'max_length[' . $max_length . ']|';
				$validation_array["max_length"] = "La valeur du champs %s est trop longue";
				$this->form_validation->set_message($name, $validation_array["max_length"]);
			}

		} elseif ($type == 'email') {
			$validations .= 'valid_email|';
			$validation_array["valid_email"] = "L'e-mail n'est pas valide";
			$this->form_validation->set_message($name, $validation_array["valid_email"]);
		}
		if (!empty($pattern)) {
			$validations .= 'regex_match[' . $pattern . ']';
			$validation_array["regex_match"] = "Le champs %s n'est pas valide.";
			$this->form_validation->set_message($name, $validation_array["regex_match"]);

		}
		$this->form_validation->set_message($name, $validations);
		$this->form_validation->set_rules(
			$name, $label,
			$validations,
			$validation_array
		);
		if (!empty($validations)) {
		}
		if (isset($_POST)) {
			$value = $_POST[$name];
		}
		//trim|required|min_length[5]|max_length[12]|valid_email|matches[password]|is_unique[users.email]
		$html = '
<div class="mdl-textfield ' . $enableJS . '" style="width: 100%;">
	<input class="mdl-textfield__input" type="' . $type . '" id="input_' . $id . '" name="' . $name . '" ' . $required . ' value="' . $value . '" ' . $autofocus . ' />
	<label class="mdl-textfield__label" for="input_' . $id . '">' . $label . '</label>
	<span class="mdl-textfield__error">' . form_error($name) . '</span>
</div>
';
		return $html;
	}

	public function textarea($label = "Champs", $name = "name.fields", $required = false, $minLenght = "", $maxLenght = "", $rows = 7, $error = "Valeur incorrect")
	{
		$id = rand(0, time());
		if (isset($_POST)) {
			$value = $_POST[$name];
		}
		$required == 'on' ? $required = 'required="required"' : '';
		$html = '
<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width: 100%;">
	<textarea class="mdl-textfield__input" id="input_' . $id . '" name="' . $name . '" rows="' . $rows . '" ' . $required . '>' . $value . '</textarea>
	<label class="mdl-textfield__label" for="input_' . $id . '">' . $label . '</label>
	<span class="mdl-textfield__error">' . $error . '</span>
</div>
';
		return $html;
	}

	public function submit($children = "", $class = "mdl-js-textfield mdl-textfield--floating-label")
	{
		$html = '<button class="' . $class . '" type="submit">' . $children . '</button>';
		return $html;
	}

	public function select($multiple = false, $children = "", $name = "", $label = "Selection")
	{
		$html = '<div class="mdl-textfield mdl-js-textfield">';
		$html .= '<select class="mdl-textfield__input">';
		$html .= $children;
		$html .= '</select>';
		$html .= '<label class="mdl-textfield__label" for="sample3">' . $label . '</label>';
		$html .= '</div>';
		return $html;
	}

	public function option($value = "", $children = "", $selected = false)
	{
		if ($selected !== false) {
			$selected = 'selected';
		} else {
			$selected = '';
		}
		$html = '<option value="' . $value . '" ' . $selected . '>';
		$html .= $children;
		$html .= '</option > ';
		return $html;
	}
}
