<?php

class Button extends MY_Model
{
	public function link($link = "http://", $alt = "Alt link", $open = array("_self", "_blank"), $children = "", $class = "mdl-button mdl-js-button mdl-button--raised")
	{
		$html = '<a href="' . $link . '" ';
		$html .= 'target="' . $open . '"';
		$html .= ' class="' . $class . '">' . $children;
		$html .= '</a>';
		return $html;
	}

	public
	function raised_button($children = "", $href = "", $open = array("_self", "_blank"), $class = "mdl-button mdl-js-button mdl-button--raised")
	{
		$html = '<a href="' . $href . '" class="' . $class . '" target="' . $open . '">' . $children . '</a>';
		return $html;
	}

	public
	function fab_button($children = "", $href = "", $colored = true, $open = array("_self", "_blank"))
	{
		$class = 'mdl-button mdl-js-button mdl-button--fab';
		if ($colored !== false) {
			$class .= ' mdl-button--colored';
		}
		$html = '<a href="' . $href . '" class="' . $class . '" target="' . $open . '">' . $children . '</a>';
		return $html;
	}

	public
	function raised_ripple_button($children = "", $href = "", $open = array("_self", "_blank"), $class = "mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect")
	{
		$html = '<a href="' . $href . '" class="' . $class . '" target="' . $open . '">' . $children . '</a>';
		return $html;
	}

	public function modal($buttonLabel = "Open dialog", $children = "")
	{
		$modal_id = rand(0, 550);
		$html = '<button type="button" class="mdl-button show-modal_' . $modal_id . '">' . $buttonLabel . '</button>';
		$html .= '<dialog class="mdl-dialog" id="modal_' . $modal_id . '">
    <div class="mdl-dialog__content">
      <p>' . $children . '
      </p>
    </div>
  </dialog>
  <script>
    var dialog = document.getElementById(\'modal_' . $modal_id . '\');
    var showModalButton = document.querySelector(\'.show-modal_' . $modal_id . '\');
    if (! dialog.showModal) {
      dialogPolyfill.registerDialog(dialog);
    }
    showModalButton.addEventListener(\'click\', function() {
      dialog.showModal();
    });
    dialog.querySelector(\'.close\').addEventListener(\'click\', function() {
      dialog.close();
    });
  </script>';
		return $html;

	}
}
