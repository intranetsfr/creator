<?php

class Scene extends MY_Model
{
	public function scene($tot = 12, $children = "")
	{
		$html = '<div id="scene"></div> <script type="module">

			import * as THREE from \'./libs/threejs/build/three.module.js\';
			import { OrbitControls } from \'./libs/threejs/examples/jsm/controls/OrbitControls.js\';
			var camera, scene, renderer;
			var mesh;

			init();
			animate();

			function init() {

				scene = new THREE.Scene();
				renderer = new THREE.WebGLRenderer( { antialias: true } );
				renderer.setPixelRatio( window.devicePixelRatio );
				renderer.setSize( window.innerWidth, window.innerHeight );
				document.getElementById("scene").appendChild( renderer.domElement );

				//

				window.addEventListener( \'resize\', onWindowResize, false );';
		$html .= $children;
		$html .= '
			}

			function onWindowResize() {

				camera.aspect = window.innerWidth / window.innerHeight;
				camera.updateProjectionMatrix();

				renderer.setSize( window.innerWidth, window.innerHeight );

			}

			function animate() {

				requestAnimationFrame( animate );
				renderer.render( scene, camera );

			}

		</script>';
		return $html;
	}

	public function camera($x = 0, $y = 0, $z = 0, $rotation_x = 0, $rotation_y = 0, $rotation_z = 0, $control = true)
	{
		$html = '
				camera = new THREE.PerspectiveCamera( 30, window.innerWidth / window.innerHeight, 1, 10000 );
				camera.position.x = ' . $x . ';
				camera.position.y = ' . $y . ';
				camera.position.z = ' . $z . ';
				camera.rotation.x = ' . $rotation_x . ';
				camera.rotation.y = ' . $rotation_y . ';
				camera.rotation.z = ' . $rotation_z . ';
				';
		if ($control !== "false") {
			$html .= '
			
				var controls = new OrbitControls( camera, renderer.domElement );
				controls.minDistance = 20;
				controls.maxDistance = 50;
				controls.maxPolarAngle = Math.PI / 2;';
		}
		return $html;
	}

	public function cube($size_x = 10, $size_y = 10, $size_z = 10, $x = 0, $y = 0, $z = 0, $rotation_x = 0, $rotation_y = 0, $rotation_z = 0, $texture = "texture.upload", $wireframe = true, $children = "")
	{
		$id = $this->id();
		$html = '
		
				var texture_' . $id . ' = new THREE.TextureLoader().load( \'' . $texture . '\' );

				var geometry = new THREE.BoxBufferGeometry( ' . $size_x . ', ' . $size_y . ', ' . $size_z . ' );
				var material = new THREE.MeshBasicMaterial( { map: texture_' . $id . ', wireframe:' . $wireframe . ' } );

				
				mesh = new THREE.Mesh( geometry, material );
				mesh.rotation.x = ' . $rotation_x . ';
				mesh.rotation.y = ' . $rotation_y . ';
				mesh.rotation.z = ' . $rotation_z . ';
				
				
				mesh.position.x = ' . $x . ';
				mesh.position.y = ' . $y . ';
				mesh.position.z = ' . $z . ';
				' . $children . '
				scene.add( mesh );
				';
		return $html;
	}

	public function ground($x = 0, $y = 0, $z = 0, $rotation_x = 0, $rotation_y = 0, $rotation_z = 0, $texture = "texture.upload", $receiveShadow = true)
	{
		$html = '
				var loader = new THREE.TextureLoader();
				var groundTexture = loader.load( \'' . $texture . '\' );
				
				groundTexture.wrapS = groundTexture.wrapT = THREE.RepeatWrapping;
				groundTexture.repeat.set( 25, 25 );
				groundTexture.anisotropy = 16;
				groundTexture.encoding = THREE.sRGBEncoding;
				/*
				*/

				var groundMaterial = new THREE.MeshLambertMaterial( { map: groundTexture, wireframe:true, color: 0xff0000} );

				var mesh = new THREE.Mesh( new THREE.PlaneBufferGeometry( 20000, 20000 ), groundMaterial );
				//mesh.position.y = ' . $y . ';
				//mesh.position.x = ' . $x . ';
				
				mesh.position.y = -10;
				mesh.rotation.x = - Math.PI / 2;
				/*
				mesh.position.z = ' . $z . ';
				
				mesh.rotation.x = ' . $rotation_x . ';
				mesh.rotation.y = ' . $rotation_y . ';
				mesh.rotation.z = ' . $rotation_z . ';
				
				*/
				mesh.receiveShadow = ' . $receiveShadow . ';
				scene.add( mesh );';
		return $html;
	}

	public function click($message = "Test")
	{
		$html = 'mesh.addEventListener("click", function(event){
			alert("' . $message . '");
		}, false)';
		return $html;
	}
}
