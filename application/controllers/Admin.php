<?php

class Admin extends CI_Controller
{
	public $components_list = array();

	private $admin_html = '<dialog class="mdl-dialog" id="propreties"><div id="propreties_content"><div id="p2" class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div></div></dialog>';
	private $default_table = "pages";

	public function __construct()
	{
		parent::__construct();
		if (empty($this->session->userdata('admin_intranets')) && $this->uri->uri_string !== 'admin/login') {
			redirect(site_url('admin/login'));
		} elseif ($this->uri->uri_string == 'admin/login') {
			$data['admin_login'] = "";
			$data['admin_password'] = "";
			if (isset($_POST['submit'])) {
				$data['admin_login'] = $admin_login = $this->input->post("admin_login");
				$data['admin_password'] = $admin_password = $this->input->post("admin_password");

				$this->form_validation->set_rules("admin_login", "admin_login", "trim|required|valid_email", array(
					"trim" => "",
					"required" => "Votre identifiant est obligatoire",
					"valid_email" => "Votre identifiant n'est pas un e-mail"
				));
				$this->form_validation->set_rules("admin_password", "admin_password", "trim|required|min_length[8]|max_length[16]", array(
					"trim" => "",
					"required" => "Votre mot de passe est obligatoire.",
					"min_length" => "Votre mot de passe est trop court.",
					"max_length" => "Votre mot de passe est trop long",
				));
				if ($this->form_validation->run() == FALSE) {
				} else {
					$data_insert = array(
						"admin_login" => $admin_login,
						"admin_password" => $admin_password,
					);
					if ($admin_login !== ' demo@intranets.fr' && $admin_password !== "demo") {
						redirect(site_url('admin/login/?token=' . md5(time())));
					} else {
						$this->session->set_userdata('admin_intranets', md5(time()));
						redirect(site_url('admin/dashboard'));
					}
				}
			} else {
			}

		}
		$this->load->dbforge();
		$this->components_list = $this->Intranet->getComponents();

		$this->session->has_userdata('admin_user');

	}

	public function login()
	{
		$data['title'] = "Connexion";
		$data['show_main'] = false;
		$this->show("login", $data);
	}

	public function index($path = "")
	{
		if (empty($path)) {
			$this->dashboard();
		}
	}

	public function edit($pages_id)
	{
		$this->db->where("pages.pages_id", $pages_id);
		$all = $this->db->get("pages");
		$data = $all->result("array")[0];
		if ($data) {
			$this->load->view("admin/editor", $data);
		}
	}

	public function dashboard()
	{
		if (isset($_POST['pages_form_add_submit'])) {
			$data['pages_form_add_input'] = $pages_form_add_input = $this->input->post("pages_form_add_input");
			$this->form_validation->set_rules("pages_form_add_input", "pages_form_add_input", "trim|required", array(
				"required" => "Ce champs est obligatoire"
			));
			if ($this->form_validation->run() == FALSE) {
			} else {
				$search_input = array(" ");
				$replace_input = array("-");
				$pages_form_add_input = wd_remove_accents(str_replace($search_input, $replace_input, $pages_form_add_input));
				$data_insert = array(
					"pages_parent" => 0,
					"pages_class" => "Page",
					"pages_function" => "page",
					"pages_path" => $pages_form_add_input,
				);
				$this->db->insert("pages", $data_insert);
				redirect(site_url("admin/dashboard"));
			}
		}
		if (isset($_POST['pages_path_update'])) {
			$current_path = $this->input->post("pages_path_current");
			$current_new = $this->input->post("pages_path_new");
			$pages_redirection = $this->input->post("pages_redirection");
			$update = array(
				"pages_path" => $current_new,
				"pages_redirection" => $pages_redirection
			);
			$this->db->where($this->default_table . "." . $this->default_table . "_path", $current_path);
			$this->db->update($this->default_table, $update);
		}
		if (isset($_GET['table_name'])) {
			$data['table_name'] = $table_name = strtolower($this->input->get("table_name"));

			if (empty($table_name)) {
				redirect(site_url("admin/dashboard"));
			} else {
				$attributes = array('ENGINE' => 'InnoDB');

				$fields = array(
					$table_name . '_id' => array(
						'type' => 'INT',
						'constraint' => 11,
						'unsigned' => TRUE,
						'auto_increment' => TRUE
					),
					$table_name . '_date_insert' => array(
						'type' => 'DATETIME',
						'constraint' => '',
					),
				);
				$this->dbforge->add_field($fields);
				$this->dbforge->add_key($table_name . '_id', TRUE);
				$this->dbforge->create_table($table_name, TRUE, $attributes);
				redirect(site_url("admin/table/" . $table_name . "#scroll-tab-2"));
			}
		}
		if (isset($_POST['action']) && $_POST['action'] == 'delete') {
			$pages_path = $this->input->post("pages_path");
			$this->db->where($this->default_table . "_path", $pages_path);
			$this->db->delete($this->default_table);
			echo "true";
			exit();
		}
		$this->db->order_by("pages.pages_id", "asc");
		$this->db->group_by("pages.pages_path");
		$pages = $this->db->get("pages");
		$data['pages'] = $pages->result("array");
		$this->show("dashboard", $data);
	}

	public function table($table, $_id = null)
	{
		//$this->getInformations($table);

		$data['table'] = $table;
		if (isset($_POST) && !empty($_POST) && isset($_POST['submit'])) {
			unset($_POST["submit"]);

			foreach ($_POST as $p_k => $p_v) {
				if ($p_k !== $table . "_id") {
					$this->form_validation->set_rules($p_k, $p_k, "required", array(
						"required" => str_replace($table . "_", "", $p_k) . " is required",
					));
				} else {
					unset($_POST[$table . "_id"]);
				}
			}
			if ($this->form_validation->run() == FALSE) {

			} else {
				$post = array();
				$avec = array('"');
				$sans = array("'");

				foreach ($_POST as $key => $value) {
					$post[$key] = str_replace($avec, $sans, $this->input->post($key));
				}
				if ($_id !== '0') {
					$this->db->where($table . "_id", $_id);
					$this->db->update($table, $post);
					$redirect = site_url("admin/table/" . $table . "/");
				} else {
					$this->db->insert($table, $post);
					$_id = $this->db->insert_id();
					$redirect = site_url("admin/table/" . $table . "/" . $this->db->insert_id());
				}
				if (isset($_FILES) && !empty($_FILES)) {
					foreach ($_FILES as $name => $upload_data) {

						if (!empty($_FILES[$name]['name'])) {
							$config['upload_path'] = './uploads/' . $table . '/' . $_id . '/';
							@mkdir($config['upload_path'], 0777, true);
							$config['upload_path'] = $config['upload_path'];

							$akamai = str_replace('./', '/', $config['upload_path']);
							$config['allowed_types'] = 'png|jpeg|jpg|gif|pdf';
							$config['max_size'] = 2048;
							$config['encrypt_name'] = TRUE;
							$config['max_size'] = '30000'; // Added Max Size
							$config['overwrite'] = TRUE;
							$config['max_width'] = 0;
							$config['max_height'] = 0;
							if (!empty($upload_data)) {
								$this->load->library('upload', $config);
								$this->upload->initialize($config);
								if (!$this->upload->do_upload($name)) {
									$error = array('error' => $this->upload->display_errors());
									e($config['upload_path']);
									e($error);
									exit();
								} else {
									$data = $this->upload->data();
									$file = site_url(str_replace('./', '', $config['upload_path']) . $data['file_name']);

									$to_update = array(
										$name => $file
									);
									$this->db->where($table . "_id", $_id);
									$this->db->update($table, $to_update);
									//$_POST["pages_value"]["src"] = $akamai . $data['file_name'];
								}
							}
						}

					}
				}
				redirect($redirect);
			}
		}
		if (isset($_POST) && !empty($_POST) && isset($_POST['component_add_submit'])) {

			$component_add_name = $this->input->post("component_add_name");
			$component_add_select = strtoupper(str_replace(".php", "", $this->input->post("component_add_select")));
			$component_add_after = $this->input->post("component_add_after");
			if (!empty($component_add_name) && !empty($component_add_select) && !empty($component_add_after)) {
				$fields = array(
					$component_add_name => array(
						'type' => $component_add_select,
						'after' => $component_add_after
					)
				);
				switch ($component_add_select) {
					case("VARCHAR"):
						$fields[$component_add_name]['constraint'] = '255';
						break;
					case("ENUM"):
						//ALTER TABLE `articles` ADD `pages_func` ENUM('true', 'false') NOT NULL DEFAULT 'false' AFTER `articles_image`;
						$fields[$component_add_name]['length'] = "'true', 'false'";
						$fields[$component_add_name]['default'] = 'false';
						$fields[$component_add_name]['null'] = FALSE;
						$fields[$component_add_name]['type'] = $component_add_select . "('true','false')";
						break;
					case("UPLOAD"):
						unset($fields[$component_add_name]);
						$fields[$component_add_name . "_upload"]['constraint'] = '255';
						$fields[$component_add_name . "_upload"]['type'] = 'VARCHAR';
						break;
				}
				$this->dbforge->add_column($table, $fields);
			}
		}
		if ($_id == 0 || $_id == null) {
			$data['title'] = 'Ajouter une nouvelle donnée';
		} else {
			$this->db->where($table . "_id", $_id);
			$data['title'] = 'Modifier';
			$data['table_data'] = $this->db->get($table)->result("array")[0];

		}
		if ($_id == null) {
			$page = empty($_GET['page']) ? 0 : $_GET['page'];
			$limit_b = 20;

			$total = $this->db->select("count(*) as count")->get($table)->result("array")[0];
			$this->load->library('pagination');
			$config['base_url'] = site_url("admin/table/" . $table);
			$config['total_rows'] = $total['count'];
			$config['enable_query_strings'] = TRUE;
			$config['per_page'] = $limit_b;
			$config['page_query_string'] = TRUE;
			$config['query_string_segment'] = 'page';
			$config['num_links'] = 3;
			$config['uri_segment'] = 3;
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			$data['table_data'] = $this->db->select("*")->limit($limit_b, $page)->get($table)->result("array");
		}
		$data['table_fields'] = json_decode(json_encode($this->db->field_data($table)), true);
		$data['components'] = getAdminComponents();

		if ($_id !== null) {
			$this->show("table_form", $data);
		} else {
			$this->show("table", $data);
		}
	}


	private function show($view, $data)
	{
		$tables = $this->db->list_tables();
		$data['tables'] = $tables;
		$data['view'] = $view;
		$this->load->view("admin/templates/header", $data);
		$this->load->view("admin/" . $view, $data);
		$this->load->view("admin/templates/footer", $data);
	}

	public function table_remove($table_name)
	{

		$this->dbforge->drop_table($table_name, TRUE);
		redirect(site_url("admin/dashboard"));
	}

	public function remove_page($page)
	{
		$table = 'pages';
		$this->db->where($table . "_path", $page);
		$this->db->delete($table);
		redirect(site_url("admin/dashboard"));
	}

	public function data($option, $table, $uid = "")
	{
		switch ($option) {
			case("remove"):
				if (!empty($uid)) {
					$this->db->where($table . "_id", $uid);
					$this->db->delete($table);
				}
				redirect(site_url("admin/table/" . $table));
				break;
			case("remove_field"):
				if (!empty($uid)) {
					$this->dbforge->drop_column($table, $uid);
				}
				redirect(site_url("admin/table/" . $table . "/#scroll-tab-2"));
				break;
			case("remove_page"):
				if (empty($uid) && !empty($_POST['pages_path'])) {
					$this->db->where($this->default_table . "_path", $this->input->post($this->default_table . "_path"));
					$this->db->delete($this->default_table);
					echo "ok";
					return $this->db->affected_rows();
				}
				break;
		}

	}


	public function main($pages_id)
	{

		$this->db->where("pages.pages_id", $pages_id);
		$result = $this->db->get("pages")->result("array");
		$path = $result[0]["pages_path"];

		$this->db->where("pages.pages_path", $path);
		$this->db->order_by("pages.pages_index", "asc");
		$data['pages_tree'] = $this->db->get($this->default_table)->result("array");
		$data['html'] = $this->admin_html . $this->admin_menu(0, 0, $data['pages_tree']);
		$data['pages_id'] = "0";
		$this->load->view("admin/page_tree", $data);

	}

	private function admin_menu($parent, $niveau, $array)
	{
		$html = "";
		$niveau_precedent = 0;
		if (!$niveau && !$niveau_precedent) {
			$html .= '<ol  class="sortable ui-sortable mjs-nestedSortable-branch mjs-nestedSortable-expanded">';
		}
		foreach ($array AS $noeud) {
			if ($parent == $noeud['pages_parent']) {
				if ($niveau_precedent < $niveau) {
					$html .= '<ol class="mjs-nestedSortable-branch" class="sortable">';
				}
				$data['noeud'] = $noeud;
				$data['values'] = (array)json_decode($noeud['pages_value'])[0];
				$data['propreties'] = $this->components_list[$noeud["pages_class"]]['functions'][$noeud["pages_function"]];
				$class = '';
				if ($data['propreties']['children'] !== '') {
					$class = 'mjs-nestedSortable-no-nesting';

				} else {
					$class = 'mjs-nestedSortable-leaf';
				}
				$html .= '<li data-id="' . $noeud['pages_id'] . '" id="item_' . $noeud['pages_id'] . '" class="' . $class . '">' . $this->load->view("admin/propreties_button", $data, true);

				$niveau_precedent = $niveau;
				$data['niveau'] = $niveau_precedent + 1;
				$next = $this->admin_menu($noeud['pages_id'], ($niveau + 1), $array);
				$html .= $next;
			}
		}
		if (($niveau_precedent == $niveau) && ($niveau_precedent != 0)) {
			$html .= "</li></ol>\n\n";

		} else if ($niveau_precedent == $niveau) {
			$html .= "</ol>";
		} else $html .= "";
		return $html;
	}


	public function proprety($pages_id = null)
	{

		$data['page'] = null;
		if ($pages_id) {

			if (!empty($_POST) || !empty($_GET)) {
				if (!empty($_GET['pages_function']) && !empty($_GET['pages_function'])) {
					$pages_parent = $this->input->get('pages_parent');
					$pages_path = $this->input->get('pages_path');
					$pages_class = $this->input->get('pages_class');
					$pages_function = $this->input->get('pages_function');
					if (!empty($pages_function) &&
						!empty($pages_parent) &&
						!empty($pages_path)) {

						$pages_values = json_encode(array($this->components_list[$pages_class]['functions'][$pages_function]));
						$data = array(
							"pages_parent" => $pages_parent,
							"pages_path" => $pages_path,
							"pages_class" => $pages_class,
							"pages_function" => $pages_function,
							"pages_value" => $pages_values
						);
						$result = $this->db->insert("pages", $data);
						$pages_id = $this->db->insert_id();
						$redirect = site_url("admin/proprety/" . $pages_id . "?reloadTree=true&reloadPreview=true&pages_id=" . $this->db->insert_id());
					}
				} elseif (isset($_POST['update'])) {
					$pages_parent = $this->input->post('pages_parent');
					$pages_id = $this->input->post('pages_id');
					$pages_class = $this->input->post('pages_class');
					$pages_path = $this->input->post('pages_path');
					$pages_label = $this->input->post('pages_label');
					$pages_value = json_encode(array($this->input->post('pages_value')));
					$data_update = array(
						"pages_value" => $pages_value,
						"pages_parent" => $pages_parent,
						"pages_class" => $pages_class,
						"pages_path" => $pages_path,
						"pages_label" => $pages_label,
					);
					$this->db->where($this->default_table . ".pages_id", $pages_id);
					$result = $this->db->update($this->default_table, $data_update);
					/*
					e($_POST);
					e($this->db->last_query());
					exit();
					*/
					$redirect = site_url("admin/proprety/" . $pages_id . "?reloadPreview=true&pages_id=" . $pages_id);

				} elseif (isset($_POST['delete'])) {
					$remove_pages_id = $this->input->post('pages_id');

					$pages_to_delete = $this->db->get($this->default_table)->result("array");
					$this->remove_page_children($remove_pages_id, 0, $pages_to_delete);
					$redirect = site_url("admin/proprety/?reloadTree=true&reloadPreview=true");
				} else {
				}
				if (!empty($redirect)) {
					redirect($redirect);
				}
			}
			$this->db->where("pages_id", $pages_id);
			$result = $this->db->get("pages");
			$data['page'] = $result->result("array")[0];
			$data['current_pages_id'] = $pages_id;
			if (isset($data['page'])) {
				$data['propreties'] = $this->components_list[$data['page']["pages_class"]]['functions'][$data['page']["pages_function"]];
				$data['values'] = json_decode($data['page']['pages_value'], true)[0];
				//$data['show_main'] = false;
				$this->show("proprety", $data);
			} else {
				echo "<html><head><script type='text/javascript'>window.parent.reloadTree(); window.parent.reloadPreview();Intranets.propretyClose();</script></head></html>";
			}
		} else {
			echo "<script type='text/javascript'>window.parent.reloadTree(); window.parent.reloadPreview();Intranets.propretyClose();</script>";

		}
	}

	private function remove_page_children($parent, $niveau, $array)
	{
		$niveau_precedent = 0;
		foreach ($array AS $noeud) {
			if ($parent == $noeud['pages_parent']) {
				$this->db->where($this->default_table . ".pages_id", $noeud['pages_id']);
				$this->remove_page_children($noeud['pages_id'], ($niveau + 1), $array);
			}
		}
		$this->db->where($this->default_table . ".pages_id", $parent);
		$this->db->delete($this->default_table);
	}

	public function pages_update_event()
	{
		$update = $this->input->post("update");
		$current_index = 0;
		$debug = '';
		$cp = 0;
		for ($i = 0; $i < count($update); $i++) {
			$item = $update[$i];
			//$debug .= 'ID ===========>' . $item['item_id'] . '<<<==============';
			if ($item['item_id'] !== '0' && $item !== null) {
				if ($item['parent_id'] == null) {
					$item["parent_id"] = 0;
				}
				$u = array(
					"pages_parent" => $item['parent_id'],
					"pages_index" => $current_index,
				);
				$this->db->where($this->default_table . "_id", $item['item_id']);
				$this->db->update($this->default_table, $u);
				if ($cp == $item['parent_id'] && $item['parent_id'] !== 0) {
					$current_index = 0;
				} else {
					$current_index++;
					$cp == $item['parent_id'];
				}
			}
		}

		echo "true";
	}

}
