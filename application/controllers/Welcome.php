<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	public $components_list = array();

	public function __construct()
	{
		parent::__construct();
		$this->components_list = $this->Intranet->getComponents();
		switch ($this->uri->uri_string){
			default:
				//echo $this->uri->uri_string;
				break;
			case("lipaf/qui-sommes-nous"):
				redirect(site_url("ipaf.html"));
				break;
			case("lipaf/nos-atouts-lexcellence-a-tous-les-niveaux"):
			case("le-metier-dagent-de-football/la-realite-du-metier"):
			case("le-metier-dagent-de-football/lacces-a-la-plateforme"):
			case("le-metier-dagent-de-football/cliches-et-idees-recues"):
			case("lipaf/le-cadre-denseignement"):
				redirect(site_url("metier-agent-de-joueur.html"));
				break;
			case("la-formation/la-formation-premium"):
				redirect(site_url("formation-en-presentiel.html"));
				break;
			case("la-formation/la-formation-a-distance"):
				redirect(site_url("formations-a-distance.html"));
				break;
			case("inscriptions-2020"):
			case("inscriptions-2020/reunion-dinformation"):
				redirect(site_url("procedure-d-admission.html"));
				break;
			case("contactez-nous"):
				redirect(site_url("contact.html"));
				break;
			case("actualites"):
				redirect(site_url("actualites.html"));
				break;

		}
	}

	public $admin = true;

	public function index($path = "index")
	{

		//e(pathinfo($this->uri->uri_string));
		//filename
		//exit();
		$p = pathinfo($this->uri->uri_string);
		if (isset($p['filename'])) {
			$path = $p['filename'];
		}
		$allow_debug = false;
		if (is_numeric($path) && !empty($path)) {
			$this->db->where("pages.pages_id", $path);
			$_page_from_id = $this->db->get("pages")->result("array");
			$this->db->where("pages.pages_path", $_page_from_id[0]["pages_path"]);
			$allow_debug = true;
		} else {
			$path = empty($path) ? 'index' : $path;
			$this->db->where("pages.pages_path", $path);
		}
		$this->db->order_by("pages.pages_index", "asc");
		$data['pages'] = $this->db->get("pages")->result("array");

		if (isset($data['pages'][0]['pages_redirection']) && !empty($data['pages'][0]['pages_redirection'])) {
			redirect($data['pages'][0]['pages_redirection']);
		}
		$html_search = array();
		$html_replace = array();
		if (empty($data['pages'])) {


			preg_match_all('!\d+!', $path, $matches);
			$id = ($matches[0][0]);
			$search = str_replace($id, "", $path);

			$this->db->where("pages.pages_parent", 0);
			$this->db->like("pages.pages_path", $search."@");

			$p = $this->db->get("pages")->result("array");
			$c = 0;
			$g= $p[$c];

			//corrections-@corrections.corrections_id
			if (!empty($g)) {
				$item = $g['pages_path'];
				$string_old = $path;
				$string_new = $item;
				$diff = $this->Intranet->get_diff($string_old, $string_new);

				$info_table = explode(".", str_replace('@', '', $diff['newDiff']));
				if ($this->db->table_exists($info_table[0])) {
					$this->db->where($info_table[0] . "." . $info_table[1], $diff['old_diff']);
					$rdb = $this->db->get($info_table[0]);
					$result = $rdb->result("array");
					if ($rdb) {
						$db = $result[0];
						if ($db && !empty($db)) {
							foreach ($db as $k => $v) {

								array_push($html_search,
									"@" . $info_table[0] . "." . $k
								);

								array_push($html_replace,
									$v
								);
							}
						}
						$this->db->order_by("pages.pages_index", "asc");
						$this->db->where("pages.pages_path", $g['pages_path']);
						$data['pages'] = $this->db->get("pages")->result("array");

					}
				}

			} else {
				redirect(site_url('404.html'));
			}
		}
		if (!empty($html_search) && !empty($html_replace)) {
			$data['html'] = str_replace($html_search, $html_replace, $this->Intranet->afficher_menu(0, 0, $data['pages'], $allow_debug));
		} else {
			$data['html'] = $this->Intranet->afficher_menu(0, 0, $data['pages'], $allow_debug);

		}
		$this->load->view("welcome_message", $data);
	}


}
