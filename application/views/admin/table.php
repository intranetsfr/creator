<style>
	a:hover, a:focus {
		color: white;
		text-decoration: none;
	}
</style>
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
	<header class="mdl-layout__header">
		<h6 align="center"><?= $table ?></h6>
		<!-- Tabs -->
		<div class="mdl-layout__tab-bar mdl-js-ripple-effect">
			<a href="#scroll-tab-1" id="link-scroll-tab-1" class="mdl-layout__tab"><i class="material-icons"
																					  style="font-size: 14px">format_list_numbered</i>
				Données</a>
			<a href="#scroll-tab-2" id="link-scroll-tab-2" class="mdl-layout__tab"><i class="material-icons"
																					  style="font-size: 14px">short_text</i>
				Structure</a>
			<a href="#scroll-tab-3" id="link-scroll-tab-3" class="mdl-layout__tab"><i class="material-icons"
																					  style="font-size: 14px">build</i>
				Options</a>
		</div>
	</header>

	<main class="mdl-layout__content">
		<section class="mdl-layout__tab-panel" id="scroll-tab-1">
			<div class="page-content">
				<div align="center">
					<br/>
					<a href="<?= site_url("admin/table/" . $table . "/0") ?>"
					   class="mdl-button mdl-button--raised mdl-button--accent mdl-button--colored">
						<i class="material-icons">add</i> Ajouter un entrée
					</a>
					<br/>
					<br/>
					<?= $pagination?>
				</div>
				<style>
					.mdl-data-table {
						border: initial;
						border-collapse: unset !important;
						/* white-space: nowrap; */
						background-color: #fff;
					}
				</style>
				<table class="mdl-data-table mdl-shadow--2dp"
					   style="">
					<thead>
					<tr>
						<?php
						$search = array("_", $table);
						$replace = array(" ", "");
						foreach ($table_fields as $field) {
							?>
							<th class="mdl-data-table__cell--non-numeric">

								<button class="mdl-button mdl-button--colored">
									<i class="material-icons">search</i>
									search in <?= strtoupper(str_replace($search, $replace, $field['name'])) ?>
								</button>

							</th>
							<?php
						}
						?>
					</tr>
					</thead>
					<tbody>
					<?php
					foreach ($table_data as $table_datum) {
						?>
						<tr>
							<?php
							foreach ($table_fields as $field) {
								?>
								<td class="mdl-data-table__cell--non-numeric">
									<?php
									if ($field['primary_key'] == true) {
										?>
										<a href="javascript:remove('<?= $table ?>', <?= $table_datum[$field['name']] ?>)"
										   class="mdl-button mdl-button--mini-fab mdl-button--fab">
											<i class="material-icons">delete</i>
										</a>
										<a href="<?= site_url("admin/table/" . $table . "/" . $table_datum[$field['name']]) ?>"
										   class="mdl-button mdl-button--mini-fab mdl-button--fab">
											<i class="material-icons">build</i>
										</a>
										<?php
									}
									$limit = 30;
									?>
									<?= substr(strip_tags($table_datum[$field['name']]), 0, $limit) ?>
									<?= strip_tags(strlen($table_datum[$field['name']]) > $limit) ? '...' : '' ?>
								</td>
								<?php
							}
							?>
						</tr>
						<?php
					}
					?>
					</tbody>
				</table>
			</div>
		</section>
		<section class="mdl-layout__tab-panel" id="scroll-tab-2">
			<div class="page-content">
				<table class="mdl-data-table mdl-shadow--2dp"
					   style="width: 100%;">
					<thead>
					<th class="mdl-data-table__cell--non-numeric">Nom du champs</th>
					<th class="mdl-data-table__cell--non-numeric">Type</th>
					<th class="mdl-data-table__cell--non-numeric">Options</th>
					</thead>
					<tbody>
					<?php
					foreach ($table_fields as $field) {
						?>
						<tr>
							<td class="mdl-data-table__cell--non-numeric" title="DB TYPE : <?= $field['type'] ?>">
								@<?= $table?>.<?= $field['name'] ?>
							</td>
							<td class="mdl-data-table__cell--non-numeric">
								<form>

									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<select class="mdl-textfield__input"
												id="select_component_<?= $field['name'] ?>" disabled>
											<?php
											foreach ($components as $component) {
												$name = str_replace('.php', '', $component);
												$selected = '';
												if ($name == $field['type']) {
													$selected = ' selected="selected"';
												}
												?>
												<option <?= $selected ?> value="<?= $component ?>"><?= $name ?></option>
												<?php
											}
											?>
										</select>
										<label class="mdl-textfield__label"
											   for="select_component_<?= $field['name'] ?>">Type actuel : </label>
									</div>
									<button class="mdl-button mdl-button--mini-fab mdl-button--disabled">Ok</button>
								</form>
							</td>
							<td class="mdl-data-table__cell--non-numeric">
								<form>
									<button class="mdl-button" type="button"
											onclick="removeField('<?= $table ?>', '<?= $field['name'] ?>')">
										<i class="material-icons">delete</i> Supprimer
									</button>
								</form>
							</td>
						</tr>
						<?php
					}
					?>
					</tbody>
					<tfoot>

					<form method="post" action="#scroll-tab-2">
						<tr style="display: block;width: 100% !important;height: 80px">
							<td>
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<input type="text" class="mdl-textfield__input" id="component_add_name"
										   value="<?= $table ?>_" autofocus name="component_add_name"/>
									<label class="mdl-textfield__label"
										   for="component_add_name">Nouveau champs : </label>
								</div>
							</td>
							<td>
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<select class="mdl-textfield__input" name="component_add_select"
											id="component_add_select">
										<option selected disabled>choisir un type:</option>
										<?php
										foreach ($components as $component) {
											$name = str_replace('.php', '', $component);
											?>
											<option <?= $selected ?> value="<?= $component ?>"><?= $name ?></option>
											<?php
										}
										?>
									</select>
									<label class="mdl-textfield__label"
										   for="component_add_select">avec ce type : </label>
								</div>
								<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
									<select class="mdl-textfield__input" name="component_add_after"
											id="component_add_after">
										<?php
										foreach ($table_fields as $field) {
											?>
											<option value="<?= $field['name'] ?>"
													selected><?= $field['name'] ?></option>
											<?php
										}
										?>
									</select>
									<label class="mdl-textfield__label"
										   for="component_add_select">après : </label>
								</div>
							</td>
							<td>
								<button class="mdl-button mdl-button--fab mdl-button--mini-fab mdl-button--colored"
										name="component_add_submit">
									<i class="material-icons">add</i>
								</button>
							</td>
						</tr>
					</form>
					</tfoot>
				</table>
			</div>
		</section>
		<section class="mdl-layout__tab-panel" id="scroll-tab-3">
			<div class="page-content">
				<div class="mdl-card">
					<div class="mdl-card__supporting-text">
						<form></form>
					</div>
					<div class="mdl-card__actions">
						<a href="<?= site_url("admin/table_remove/" . $table) ?>" class="mdl-button"><i
								class="material-icons">delete</i>
							Supprimer cette table</a>

					</div>
				</div>
			</div>
		</section>
		<section class="mdl-layout__tab-panel" id="scroll-tab-4">
			<div class="page-content"><!-- Your content goes here --></div>
		</section>
		<section class="mdl-layout__tab-panel" id="scroll-tab-5">
			<div class="page-content"><!-- Your content goes here --></div>
		</section>
		<section class="mdl-layout__tab-panel" id="scroll-tab-6">
			<div class="page-content"><!-- Your content goes here --></div>
		</section>
	</main>
</div>
<style>
	* {
		-webkit-transition: all .3s;
		-moz-transition: all .3s;
		-ms-transition: all .3s;
		-o-transition: all .3s;
		transition: all .3s;
	}
</style>
<script>
    function remove(table, uid) {
        if (confirm("Êtes-vous sur de vouloir supprimer cette entrée ?")) {
            window.location = '<?= site_url("admin/data/remove/")?>' + table + "/" + uid;
        }
    }

    function removeField(table, uid) {
        if (confirm("Êtes-vous sur de vouloir supprimer cette entrée ?")) {
            window.location = '<?= site_url("admin/data/remove_field/")?>' + table + "/" + uid;
        }
    }

    function init() {
        var scroll_tab_name = window.location.hash.replace("#", "");
        if (scroll_tab_name == "") {
            scroll_tab_name = 'scroll-tab-1';
        }
        try {
            document.getElementById(scroll_tab_name).className = document.getElementById(scroll_tab_name).className + " is-active";
            document.getElementById("link-" + scroll_tab_name).className = document.getElementById("link-" + scroll_tab_name).className + " is-active";
        } catch (e) {
            console.log(e);
        }
    }

    init();
</script>
