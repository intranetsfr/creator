<!DOCTYPE html>
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="fr"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="fr"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Intranets Admin</title>
	<script>
		var base_url = '<?= site_url()?>'
	</script>

	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"/>
	<link rel="stylesheet" href="<?= site_url('libs/bootstrap-grid/css/bootstrap.min.css') ?>"/>
	<link
		href='https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic|Roboto+Slab:400,700|Inconsolata:400,700&subset=latin,cyrillic'
		rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?= site_url('libs/material-design-lite/material.min.css') ?>" type="text/css"/>
	<link rel="stylesheet" href="<?= site_url("libs/select2/dist/css/select2.min.css") ?>"/>
	<link rel="stylesheet" href="<?= site_url('libs/fr.intranets/css/admin.css?time' . time()) ?>" type="text/css"/>



</head>

<body class="wy-body-for-nav" role="document">
<!-- Uses a transparent header that draws on top of the layout's background -->
<style>
	.wy-body-for-nav {
		background: url('<?= site_url('libs/fr.intranets/images/transparent.jpg')?>') center / cover;
		background-position: 100% 100%;
		background-color: black;
	}

	.demo-layout-transparent .mdl-layout__header,
	.demo-layout-transparent .mdl-layout__drawer-button {
		/* This background is dark, so we set text to white. Use 87% black instead if
		   your background is light. */
		color: white;
	}


	a:hover {
		text-decoration: none;
	}
</style>

<div class="demo-layout-transparent mdl-layout mdl-js-layout">
	<?php
	if ($show_main !== false) {
		?>
		<header class="mdl-layout__header mdl-layout__header--transparent">
			<div class="mdl-layout__header-row">
				<!-- Title -->
				<span class="mdl-layout-title">&lt;/intranets&gt;</span>
				<!-- Add spacer, to align navigation to the right -->
				<div class="mdl-layout-spacer"></div>
				<!-- Navigation -->
				<nav class="mdl-navigation">
					<a class="mdl-navigation__link" href="">Documentation</a>
				</nav>
			</div>
		</header>
		<div class="mdl-layout__drawer">
			<span class="mdl-layout-title">&lt;/intranets&gt;</span>
			<nav class="mdl-navigation">
				<a class="mdl-navigation__link" href="<?= site_url("admin/dashboard") ?>">
					<strong><i class="material-icons">home</i>
						Tableau de bord
					</strong>
				</a>
				<a class="mdl-navigation__link" href="javascript:void(0)" id="show-dialog" type="button">
					<i class="material-icons">add</i>
					Créer un tableau de données</a>
				<hr/>
				<h6>Les données actuelles : </h6>
				<?php
				foreach ($tables as $table) {
					if ($table !== 'pages') {
						?>
						<a class="mdl-navigation__link"
						   href="<?= site_url('admin/table/' . $table) ?>"><?= $table ?></a>
						<?php
					}
				}
				?>
			</nav>
		</div>
		<form action="<?= site_url("admin/dashboard") ?>" method="get">
			<dialog class="mdl-dialog" style="width: 50%;">
				<div class="mdl-dialog__title">Ajouter une nouvelle table :</div>
				<div class="mdl-dialog__content">
					<div class="mdl-textfield mdl-js-textfield">
						<input class="mdl-textfield__input" type="text" pattern="^[a-zA-Z0-9]*$" name="table_name"
							   id="table_name">
						<label class="mdl-textfield__label" for="table_name">Nom de votre table : </label>
						<span class="mdl-textfield__error">
						Ce nom n'est pas valide. <strong>Seulement : </strong>
						<ul>
							<li>sans espace</li>
							<li>sans accent</li>
							<li>caractères spéciaux</li>
						</ul>
					</span>
					</div>
				</div>
				<div class="mdl-dialog__actions">
					<button type="submit" class="mdl-button">Valider</button>
					<button type="button" class="mdl-button close">Fermer</button>
				</div>
			</dialog>
		</form>
		<script>
			var dialog = document.querySelector('dialog');
			var showDialogButton = document.querySelector('#show-dialog');
			if (!dialog.showModal) {
				dialogPolyfill.registerDialog(dialog);
			}
			showDialogButton.addEventListener('click', function () {
				dialog.showModal();
			});
			dialog.querySelector('.close').addEventListener('click', function () {
				dialog.close();
			});
		</script>
		<?php
	}
	?>
	<main class="mdl-layout__content">
