</main>
</div>

<script src="<?= site_url('libs/material-design-lite/material.min.js') ?>"></script>
<script src="<?= site_url('libs/jquery/dist/jquery.min.js') ?>"></script>
<script type="text/javascript" src="<?= site_url("libs/select2/dist/js/select2.min.js") ?>"></script>
<script src="<?= site_url('libs/fr.intranets/js/fr.intranets.js?time=' . time()) ?>"></script>

<script>
	$(document).ready(function () {
		$("select2").select2();

	});
</script>
</body>

</html>
