<style>
	.mdl-textfield {
		width: 100%;
	}
</style>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<br/>
			<?php echo form_open_multipart('', array(
				"method"=>"POST"
			));?>
				<div class="mdl-card" style="width: 100%">
					<div class="mdl-card__title">
						<h4><?= $title ?></h4>
					</div>
					<div style="position: relative;">
						<div class="mdl-textfield__error" style="right: 0px;visibility: visible !important;">
							<?= validation_errors() ?>
						</div>
					</div>
					<div class="mdl-card__supporting-text">
						<div class="row">
							<div class="col-xs-6">
							</div>
						</div>
						<?php
						foreach ($table_fields as $field) {
							?>
							<div class="col-xs-12">
								<?php
								$d = $field;
								$d['field_name'] = str_replace('_', ' ', $field['name']);
								if (count($table_data) > 0) {
									$d['value'] = $table_data[$field['name']];
								}

								//if(preg_match('image)', $field['field_name']) == 1) {
								if (strpos($field['name'], '_upload') !== false) {
									$this->load->view("admin/components/upload", $d);
								}else{
									$this->load->view("admin/components/" . $field['type'], $d);

								}
								//$this->load->view("admin/components/" . $field['type'], $d);
								?>
							</div>
							<?php
						}
						?>
						<div class="mdl-card__actions" align="center">
							<a href="<?= site_url("admin/table/" . $table) ?>">retour</a>
							<button class="mdl-button mdl-button--raised mdl-button--colored" name="submit">
								<i class="material-icons">check</i>
								Valider
							</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
