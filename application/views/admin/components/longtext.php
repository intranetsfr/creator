<div class="mdl-textfield mdl-js-textfield  mdl-textfield--floating-label" style="display block; width: 100%;">
	<textarea class="mdl-textfield__input tinymce"
			  placeholder="<?= $name ?>"
			  rows="10"
			  name="<?= $name ?>"
			  id="sample_pages_value_<?= $name ?>"><?= $value ?></textarea>
	<label class="mdl-textfield__label"
		   for="sample_pages_value_<?= $name ?>"><?= $name ?>
		: </label>
	<span
		class="mdl-textfield__error"><!-- use pattern in input --></span>
</div>
