<div class="mdl-textfield mdl-js-textfield  mdl-textfield--floating-label" style="display block; width: 100%;">
	<textarea class="mdl-textfield__input tinymce"
			  placeholder="<?= $name ?>"
			  rows="10"
			  name="<?= $name ?>"
			  id="sample_pages_value_<?= $name ?>"><?= $value ?></textarea>
	<label class="mdl-textfield__label"
		   for="sample_pages_value_<?= $name ?>"><?= $name ?>
		: </label>
	<span
		class="mdl-textfield__error"><!-- use pattern in input --></span>
</div>

<script src="<?= site_url("libs/tinymce/tinymce.min.js") ?>"></script>

<script type="text/javascript">

	document.addEventListener('mdl-componentupgraded', function (e) {
		if (typeof e.target.MaterialLayout !== 'undefined') {
			tinymce.init({
				selector: '.tinymce',
				menubar: false,
				language: 'fr_FR',
				plugins: 'code | link | importcss',
				content_css: "<?= site_url('/libs/ipaf/css/ipaf.css')?>",
				toolbar: 'removeformat | undo redo | code | styleselect | lists textcolor bold italic underline strikethrough superscript subscript codeformat | formats blockformats fontformats fontsizes align | forecolor backcolor  | alignleft aligncenter alignright alignjustify | link | forecolor backcolor | outdent indent',

			});
		}

	});
</script>
