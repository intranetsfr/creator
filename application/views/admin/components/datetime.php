<div class="mdl-textfield mdl-js-textfield  mdl-textfield--floating-label">
	<input type="text" class="mdl-textfield__input" pattern="(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})"
		   name="<?= $name ?>" placeholder="YYYY-mm-dd HH:ii:s" id="input_<?= $name ?>"
		   value="<?= empty($value) ? date("Y-m-d H:i:s") : $value ?>"/>
	<label class="mdl-textfield__label" for="input_<?= $name ?>"><?= $field_name ?> : </label>
	<span class="mdl-textfield__error">this value isn't correct !</span>
</div>
