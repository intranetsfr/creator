<?php
if ($primary_key) {
	?>
	<input type="hidden" name="<?= $name?>" value="<?= $value?>"/>
	<?php
} else {
	?>
	<div class="mdl-textfield mdl-js-textfield  mdl-textfield--floating-label">
		<input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" name="<?= $name ?>" value="<?= $value?>" id="input_<?= $name ?>">
		<label class="mdl-textfield__label" for="input_<?= $name ?>"><?= $field_name ?> : </label>
		<span class="mdl-textfield__error">Input is not a number!</span>
	</div>
	<?php
}
?>
