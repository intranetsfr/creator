<div class="mdl-textfield mdl-js-textfield  mdl-textfield--floating-label">
	<input class="mdl-textfield__input" type="text" id="input_<?= $name ?>" name="<?= $name ?>" value="<?= $value?>" />
	<label class="mdl-textfield__label" for="input_<?= $name ?>"><?= $field_name ?> : </label>
	<span class="mdl-textfield__error">this value isn't correct !</span>
</div>
