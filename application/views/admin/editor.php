<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<html>
<head>
	<link rel="stylesheet" href="<?= site_url("libs/fr.intranets/css/editor.css?time=" . time()) ?>"/>
	<script type="text/javascript" src="<?= site_url("libs/jquery/dist/jquery.min.js") ?>"></script>
	<script type="text/javascript">
		function reloadTree() {
			document.getElementById("page_tree").contentWindow.location.reload();
		}

		function reloadPreview(pages_id) {
			if (typeof pages_id !== "undefined") {
				document.getElementById("page_render").contentWindow.location = "<?= site_url($pages_id . ".intranets#")?>" + pages_id;
			} else {
				document.getElementById("page_render").contentWindow.location.reload();
			}
		}

		function reloadTree() {
			document.getElementById("page_tree").contentWindow.location.reload();
		}

		function reloadProprety(pages_id) {
			document.getElementById("proprety").contentWindow.location = "<?= site_url("admin/proprety/")?>" + pages_id;
		}

		var Editor = {};
		Editor.init = function () {
		};
		Editor.showProps = function (show, pages_id) {
			$(function () {
				if (show == true) {
					size = '*,70%';
					reloadProprety(pages_id);
					reloadPreview(pages_id);
				} else {
					size = '100%,0%';
				}
				$("#propreties").attr("rows", size)
			});
		};
		Editor.element = function (visible, pages_id) {
			try {
				var element = document.getElementById("page_render").contentWindow.document.getElementById("element_" + pages_id);
				if (visible) {
					element.classList = "debug hover";
					document.getElementById("page_render").contentWindow.location.hash = "#link_" + pages_id;
				} else {
					element.classList = "debug";
				}

			} catch (error) {

			}
		};
		var i = 0;
		$(document).ready(function () {
			$("#page_render").load(function () {
				alert(i);
			});
		});

	</script>
</head>
<frameset cols="50%,50%">
	<frame src="<?= site_url($pages_id . ".intranets") ?>" id="page_render"/>
	<frameset rows="*,0%" id="propreties">
		<frame src="<?= site_url("admin/main/" . $pages_id) ?>" id="page_tree"/>
		<frame src="" id="proprety"/>
	</frameset>
</frameset>
</html>
