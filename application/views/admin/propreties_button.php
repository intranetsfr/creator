<div class="menuDiv"
	 onmouseover="window.parent.Editor.element(true,<?= $noeud['pages_id'] ?>)"
	 onmouseout="window.parent.Editor.element(false, <?= $noeud['pages_id'] ?>)">
	<span data-id="<?= $noeud['pages_id'] ?>" class="itemTitle"></span>
	<span>
		<?php
		$search = array("_");
		$replace = array(" ");
		?>
		<?= $noeud['pages_class'] ?> / <?= ucfirst(str_replace($search, $replace, $noeud['pages_function'])) ?>
		<?php
		if (!empty($noeud['pages_label'])) {
			echo '<i>(' . $noeud['pages_label'] . ')</i>';
		}
		?>
	</span>

	<a href="javascript:void(0)" class="mdl-button mdl-js-button"
	   onclick="window.parent.Editor.showProps(true, <?= $noeud['pages_id'] ?>)">
		<i class="material-icons">build</i>
	</a>
</div>
