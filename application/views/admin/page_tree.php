<!DOCTYPE html>
<html lang="fr" dir="ltr">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="<?= site_url('libs/material-design-lite/material.min.css') ?>" type="text/css"/>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"/>
	<link rel="stylesheet" href="<?= site_url('libs/bootstrap-grid/css/bootstrap.min.css') ?>"/>
	<link href="<?= site_url('libs/fr.intranets/css/tree.css?time=' . time()) ?>"
		  rel="stylesheet" type="text/css"/>
	<script>
        var base_url = '<?= site_url()?>';
	</script>
</head>
<body>
<?php
echo $html;
?>
<div id="serializeOutput"></div>
<script type="text/javascript"
		src="<?= site_url('libs/jquery/dist/jquery.min.js') ?>"></script>
<script type="text/javascript"
		src="<?= site_url('libs/jquery-ui/jquery-ui.min.js')?>"></script>
<script type="text/javascript"
		src="<?= site_url('libs/nestedSortable/jquery.ui.nestedSortable.js') ?>"></script>

<script src="<?= site_url('libs/fr.intranets/js/fr.intranets.js?time=' . time()) ?>"></script>
<script type="text/javascript">
	var root_pages_id = <?= $pages_id?>;
</script>
<script type="text/javascript"
		src="<?= site_url('libs/fr.intranets/js/tree.js?time=' . time()) ?>"></script>
<?php
if(isset($_GET['reloadPreview']) && $_GET['reloadPreview'] == 'true'){
	?>
	<script>Intranets.ReloadPreview()</script>
<?php
}
?>
</body>
</html>
