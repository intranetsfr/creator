<div class="container-fluid">
	<div class="row">

		<div class="col-xs-12">
			<div class="mdl-card" style="width: 100%">
				<form method="post">
					<h4 align="center" class="mdl-card__title">Ajouter une nouvelle page</h4>
					<?php
					$errors = validation_errors();
					if (!empty($errors)) {
						?>
						<div class="mdl-card__title" style="color: red;">
							<?= validation_errors() ?>
						</div>
						<?php
					}
					?>
					<div class="mdl-card__title">
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
							<input class="mdl-textfield__input" type="text" pattern="^\S*$"
								   id="pages_form_add_input"
								   placeholder="votre-url" name="pages_form_add_input">
							<label class="mdl-textfield__label" for="pages_form_add_input"><?= site_url() ?></label>
							<span class="mdl-textfield__error">Cette url n'est pas valide</span>
						</div>
						<span>.html</span>
						</p>
					</div>
					<div class="mdl-card__actions">
						<button type="submit" name="pages_form_add_submit"
								class="mdl-button mdl-button--raised mdl-button--colored">
							<i class="fa fa-plus"></i>
							Ajouter
						</button>
						<br/>
						<br/>
					</div>
				</form>
			</div>
		</div>
		<div class="col-xs-12">
			<br/>
			<div class="mdl-card" style="width: 100%">
				<h4 align="center" class="mdl-card__title">Mes pages</h4>
				<table class="mdl-data-table mdl-shadow--2dp" width="100%">

					<thead>
					<tr>
						<th class="mdl-data-table__cell--non-numeric">Nom de votre page</th>
						<th class="mdl-data-table__cell--non-numeric">URL</th>
						<th class="mdl-data-table__cell--non-numeric">Redirection</th>
						<th class="mdl-data-table__cell--non-numeric">Options</th>
					</tr>
					</thead>
					<tbody>

					<?php
					foreach ($pages as $page) {
						?>
						<tr>
							<td class="mdl-data-table__cell--non-numeric">
								<?= $page['pages_label'] ?>
							</td>
							<form method="post">
								<td class="mdl-data-table__cell--non-numeric">
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<input type="hidden" pattern="^\S*$"
											   id="form_pages_pages_current_path_<?= $page['pages_id'] ?>"
											   value="<?= $page['pages_path'] ?>"
											   placeholder="votre-url" name="pages_path_current"/>
										<input class="mdl-textfield__input" type="text" pattern="^\S*$"
											   id="form_pages_pages_path_<?= $page['pages_id'] ?>"
											   value="<?= $page['pages_path'] ?>"
											   placeholder="votre-url" name="pages_path_new"/>
										<label class="mdl-textfield__label"
											   for="pages_form_add_input"><?= site_url() ?></label>
										<span class="mdl-textfield__error">Cette url n'est pas valide</span>
									</div>
									<span>.html</span>
								</td>
								<td>
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<input class="mdl-textfield__input" type="text" pattern="^\S*$"
											   value="<?= $page['pages_redirection'] ?>"
											   placeholder="votre-url" name="pages_redirection"/>
										<label class="mdl-textfield__label"
											   for="pages_form_add_input">Vers : </label>
										<span class="mdl-textfield__error">Cette url n'est pas valide</span>
									</div>

									<button class="mdl-button  mdl-button--raised mdl-button--colored"
											name="pages_path_update">
										<i class="material-icons">save</i> Enregistrer
									</button>
								</td>
							</form>
							<td class="mdl-data-table__cell--non-numeric">
								<div class="mdl-card__actions">

									<!--a href="#" target="_blank"
										   class="mdl-button mdl-button--mini-fab mdl-button--fab"
										   id="copie_<?= $page['pages_id'] ?>">
											<i class="material-icons">file_copy</i>
										</a-->
									<div class="mdl-tooltip" data-mdl-for="copie_<?= $page['pages_id'] ?>">
										Copier cette page
									</div>
									<a href="javascript:Intranets.PageDelete('<?= $page['pages_path'] ?>')"
									   class="mdl-button mdl-button--mini-fab mdl-button--fab mdl-button__warn"
									   id="delete_<?= $page['pages_id'] ?>">
										<i class="material-icons">delete</i>
									</a>
									<div class="mdl-tooltip" data-mdl-for="delete_<?= $page['pages_id'] ?>">
										Supprimer cette page
									</div>
									<a href="<?= site_url("admin/edit/" . $page['pages_id']) ?>" target="_blank"
									   class="mdl-button mdl-button--mini-fab mdl-button--fab"
									   id="edit_<?= $page['pages_id'] ?>">
										<i class="material-icons">build</i>
									</a>
									<div class="mdl-tooltip" data-mdl-for="edit_<?= $page['pages_id'] ?>">
										Modifier cette page
									</div>
									<a href="<?= site_url($page['pages_path']) ?>.html" class="mdl-button"
									   target="_blank">voir la page</a>
								</div>
							</td>
						</tr>
						<?php
					}
					?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
