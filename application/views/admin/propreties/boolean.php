<div class="mdl-textfield">
	<label
		class="mdl-mdl-textfield mdl-js-textfield  mdl-textfield--floating-label  mdl-textfield--floating-labelis-dirty is-upgraded"
		for="input_<?= $name ?>"><?= $name ?> : </label>
	<select id="input_<?= $name ?>" name="pages_value[<?= $name ?>]" class="mdl-textfield__input">
		<option value="false" <?= $value == 'true' ? 'selected' : '' ?>>false</option>
		<option value="true" <?= $value == 'true' ? 'selected' : '' ?>>true</option>
	</select>
	<span class="mdl-textfield__error">this value isn't correct !</span>
</div>
