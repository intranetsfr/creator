<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	<textarea class="mdl-textfield__input" type="file"
		   placeholder="<?= $name ?>"
		   name="pages_value[<?= $name ?>]"
			  id="sample_pages_value_<?= $name ?>"><?= $values[$name] ?></textarea>
	<label class="mdl-textfield__label"
		   for="sample_pages_value_<?= $name ?>"><?= $name ?>
		: </label>
	<span
		class="mdl-textfield__error"><!-- use pattern in input --></span>
</div>
