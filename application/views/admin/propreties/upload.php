<div style="width: 100%">
	<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
		<input class="mdl-textfield__input" type="file"
			   placeholder="<?= $name ?>"
			   name="<?= $name ?>"
			   id="sample_pages_value_<?= $name ?>_upload">
		<label class="mdl-textfield__label"
			   for="sample_pages_value_<?= $name ?>_upload">
			Rechercher un fichier pour <?= $name ?>
			: </label>
		<span class="mdl-textfield__error"><!-- use pattern in input --></span>
	</div>
	<?php
	if (!empty($default)) {
		?>
		<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			<input class="mdl-textfield__input" type="text"
				   placeholder="URL "
				   value="<?= $value ?>"
				   name="pages_value[<?= $name ?>]"
				   id="sample_pages_value_<?= $name ?>">
			<label class="mdl-textfield__label"
				   for="sample_pages_value_<?= $name ?>">
				Coller votre <?= $name ?> URL ici (CTRL +V )
				: </label>
			<span
				class="mdl-textfield__error"><!-- use pattern in input --></span>
		</div>
		<?php
		if (!empty($value)) {
			?>
			<pre><?= $value ?></pre>

			<?php
		}
	}
	?>
</div>
