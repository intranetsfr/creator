<?php
$tables = $this->db->list_tables();
$data['tables'] = $tables;
$default = str_replace(".sql", "", $default);
?>
<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	<select
		name="pages_value[<?= $name ?>]"
		class="mdl-textfield__input"
		id="sample_pages_value[<?= $name ?>]">
		<option <?= empty($default) ? 'selected' : '' ?>></option>
		<?php
		foreach ($tables as $table) {
			if ($table !== 'pages') {

				$selected = '';
				if ($table == $default) {
					$selected = 'selected="selected"';
				}
				if ($table == $value) {
					$selected = 'selected="selected"';
				}
				?>
				<option label="<?= $table ?>" <?= $selected ?> value="<?= $table ?>"><?= $table ?></option>
				<?php
			}
		}
		?>
	</select>

	<label class="mdl-textfield__label" for="input_<?= $name ?>"><?= $name ?> : </label>
	<span class="mdl-textfield__error">this value isn't correct !</span>
</div>
