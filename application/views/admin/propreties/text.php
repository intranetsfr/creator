<div class="" style="display block; width: 100%;">
	<label class=""
		   for="sample_pages_value_<?= $name ?>"><?= $name ?>
		: </label>
	<textarea class=" tinymce"
			  placeholder="<?= $name ?>"
			  rows="20"
			  name="pages_value[<?= $name ?>]"
			  id="sample_pages_value_<?= $name ?>"><?= $value ?></textarea>
	<span
		class=""><!-- use pattern in input --></span>
</div>

<script src="<?= site_url("libs/tinymce/tinymce.min.js") ?>"></script>

<script type="text/javascript">

	document.addEventListener('mdl-componentupgraded', function (e) {
		if (typeof e.target.MaterialLayout !== 'undefined') {
			tinymce.init({
				selector: '.tinymce',
				menubar: false,
				language: 'fr_FR',
				plugins: 'code | link | importcss',
				content_css: "<?= site_url('/libs/ipaf/css/ipaf.css')?>",
				toolbar: 'removeformat | undo redo | code | styleselect | lists textcolor bold italic underline strikethrough superscript subscript codeformat | formats blockformats fontformats fontsizes align | forecolor backcolor  | alignleft aligncenter alignright alignjustify | link | forecolor backcolor | outdent indent',

			});
		}

	});
</script>
