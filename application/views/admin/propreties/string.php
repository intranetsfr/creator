<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	<input class="mdl-textfield__input" type="text"
		   placeholder="<?= $name ?>"
		   name="pages_value[<?= $name ?>]"
		   value="<?= $value ?>"
		   id="sample_pages_value_<?= $name ?>">
	<label class="mdl-textfield__label"
		   for="sample_pages_value_<?= $name ?>">
		<?= $name ?> :
	</label>
	<span
		class="mdl-textfield__error"><!-- use pattern in input --></span>
</div>
