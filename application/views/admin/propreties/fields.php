<?php
$tables = $this->db->list_tables();
$data['tables'] = $tables;
$default = str_replace(".sql", "", $default);
?>
<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	<select
		name="pages_value[<?= $name ?>]"
		class="mdl-textfield__input"
		id="sample_pages_value[<?= $name ?>]">
		<option <?= empty($default) ? 'selected' : '' ?>></option>
		<?php
		foreach ($tables as $table) {
			if ($table !== 'pages') {

				$selected = '';
				if ($table == $default) {
					$selected = 'selected="selected"';
				}
				if ($table == $value) {
					$selected = 'selected="selected"';
				}
				?>
				<optgroup label="<?= $table ?>" <?= $selected ?> value="<?= $table ?>"><?= $table . $value ?>
					<?php
					$fields = $this->db->field_data($table);

					foreach ($fields as $field) {
						$selected_option = "";
						if ($value == $table . "[" . $field->name . "]") {
							$selected_option = "selected";
						}
						?>
						<option value="<?= $table . "[" . $field->name . "]" ?>"
								<?= $selected_option ?>><?= $table . "_" . $field->name ?></option>
						<?php
						/*
						echo $field->name;
						echo $field->type;
						echo $field->max_length;
						echo $field->primary_key;
						**/
					}
					?>
				</optgroup>
				<?php
			}
		}
		?>
	</select>

	<label class="mdl-textfield__label" for="input_<?= $name ?>"><?= $name ?> : </label>
	<span class="mdl-textfield__error">this value isn't correct !</span>
</div>
