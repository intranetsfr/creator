<?php
if (!empty($default)) {
	?>
	<div class="col-xs-6">
		<label class="" for="sample_pages_value[<?= $name ?>]"><?= $name ?> : </label>
		<select class="select2"
				name="pages_value[<?= $name ?>]"
				id="sample_pages_value[<?= $name ?>]">
			<?php
			foreach ($default as $_k => $_v) {
				?>
				<option value="<?= $_v ?>" <?= $_v == $value ? 'selected="selected"' : '' ?>>
					<?= $_v ?>
				</option>
				<?php
			}
			?>
		</select>
	</div>
	<?php
} else {
	echo "empty";
}
