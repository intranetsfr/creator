<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
	<select class="mdl-textfield__input"
			placeholder="<?= $name ?>"
			name="pages_value[<?= $name ?>]"
			id="sample_pages_value_<?= $name ?>_select">
		<option value=""></option>
		<?php
		$this->db->where("pages.pages_parent !=", 0);
		$this->db->where("pages.pages_label !=", "");
		$templates = $this->db->get("pages")->result("array");
		foreach ($templates as $template) {
			?>
			<option <?= $values[$name] == $template['pages_id'] ? 'selected' : '' ?>
					value="<?= $template['pages_id'] ?>"><?= $template['pages_label'] ?> ( <?= $template['pages_path'] ?> )</option>
			<?php
		}
		?>
	</select>
	<label class="mdl-textfield__label"
		   for="sample_pages_value_<?= $name ?>_select"><?= $name ?>
		: </label>
	<span
		class="mdl-textfield__error"><!-- use pattern in input --></span>
</div>
