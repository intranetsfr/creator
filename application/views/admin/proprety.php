<div class="container-fluid">
	<br/>
	<div id="dialog_<?= $page['pages_id'] ?>" class="mdl-card" style="width: 100%">
		<div class="mdl-dialog__content">
			<div class="row">
				<div class="col-xs-12" align="right">
					<form method="post">
						<input type="hidden" name="pages_id" value="<?= $page['pages_id'] ?>">
						<button name="delete" class="mdl-button mdl-js-button" onclick="window.parent.Editor.showProps(false);"
								id="pages_remove_button_<?= $page['pages_id'] ?>"><i class="material-icons">delete</i>
						</button>
						<div class="mdl-tooltip mdl-tooltip--bottom mdl-tooltip--large "
							 for="pages_remove_button_<?= $page['pages_id'] ?>">
							Supprimer ce composant
						</div>
						<a href="#pages_id_<?= $page['pages_id'] ?>" class="mdl-button mdl-js-button mdl-button--raised"
						   onclick="Intranets.propretyClose(<?= $page['pages_id'] ?>)">
							fermer
						</a>
					</form>
				</div>
				<div class="col-xs-12" align="center">
					<h5><?= $page['pages_class'] ?> / <?= $page['pages_function'] ?></h5>
				</div>
				<div class="col-xs-12">
					<?php
					echo form_open_multipart('admin/proprety/' . $page['pages_id'], array("method" => "POST", "autocomplete" => "off")); ?>
					<div class="row">
						<input type="hidden" name="pages_parent" value="<?= $page['pages_parent'] ?>"/>
						<input type="hidden" name="pages_id" value="<?= $page['pages_id'] ?>"/>
						<input type="hidden" name="pages_path" value="<?= $page['pages_path'] ?>"/>
						<input type="hidden" name="pages_class" value="<?= $page['pages_class'] ?>"/>
						<?php
						$hasChildren = false;
						if ($propreties) {
							foreach ($propreties as $proprety_name => $proprety_value) {
								$file_edit = 'admin/propreties/' . ($proprety_name) . (empty($proprety_value) ? '_empty' : '');
								if (empty($proprety_value) && $proprety_name == 'children') {
									$hasChildren = true;
									?><input type="hidden" name="pages_value[children]" value=""/>
									<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
										<input class="mdl-textfield__input" type="text"
											   placeholder=""
											   name="pages_label"
											   value="<?= $page['pages_label'] ?>"
											   id="pages_label">
										<label class="mdl-textfield__label"
											   for="pages_template">
											<?php
											if ($page['pages_class'] == 'Page' && $page['pages_function'] == 'page') {
												echo 'Nom de la page :';
											} else {
												echo 'Nom du template :';
											}
											?>
										</label>
										<span
											class="mdl-textfield__error"><!-- use pattern in input --></span>
									</div>
									<?php
								} else {
									$p = pathinfo($proprety_value);
									$type = gettype($proprety_value);
									$data_p['name'] = $proprety_name;
									$data_p['default'] = $proprety_value;
									$data_p['value'] = $values[$proprety_name];
									if (!empty($p['extension'])) {
										$this->load->view("admin/propreties/" . $p['extension'], $data_p);
									}else{
										$this->load->view("admin/propreties/" . $type, $data_p);
									}

								}
							}
						}
						?>
					</div>
					<div align="center">
						<button name="update" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored"><i
								class="material-icons">update</i> Mettre à jour
						</button>
					</div>
					</form>
				</div>
				<?php
				if ($hasChildren) {
					?>
					<div class="col-xs-12">
						<h6>Ajouter un nouveau composant </h6>
						<div class="intranets_components_list">
							<?php
							foreach ($this->components_list as $k => $v) {
								?>
								<h6><?= $k ?></h6>
								<?php
								foreach ($v['functions'] as $f_n => $f_v) {
									$url = "admin/proprety/" . $page['pages_id'] . "?";
									$url .= 'pages_parent=' . $page['pages_id'];
									$url .= '&pages_path=' . $page['pages_path'];
									$url .= '&pages_class=' . $k;
									$url .= '&pages_function=' . $f_n;

									?>
									<!-- Contact Chip -->
									<a href="<?= site_url($url) ?>" class="mdl-button mdl-button--raised">
										<?= ucfirst(str_replace('_', ' ', $f_n)); ?>
									</a>
									<?php
								}
								?>
								<hr/>
								<?php
							}
							?>
						</div>
					</div>
					<?php
				}
				?>
			</div>
		</div>
	</div>
</div>

<script typeof="text/javascript" src="<?= site_url("libs/jquery/dist/jquery.min.js") ?>"></script>
<script typeof="text/javascript" src="<?= site_url("libs/select2/dist/js/select2.min.js") ?>"></script>
<script>
	$(".select2").select2();
</script>
<?php
if ($_GET['reloadTree']) {
	?>
	<script type="text/javascript">
		window.parent.reloadTree();
	</script>
	<?php
}
if ($_GET['reloadPreview']) {
	?>
	<script type="text/javascript">
		window.parent.reloadPreview();
	</script>
	<?php
}
?>
