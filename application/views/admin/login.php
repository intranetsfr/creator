<div class="container">
	<div class="row">
		<div class="col-xs-12 col-md-6 col-xs-offset-3">
			<br/>
			<form method="post" action="">
				<div class="mdl-card" style="width: 100%;" align="center">
					<?php
					$errors = validation_errors();
					if(!empty($errors)){
						echo $errors;
					}
					?>
					<div class="mdl-card__supporting-text">
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
							<input class="mdl-textfield__input" type="email" id="admin_login" name="admin_login">
							<label class="mdl-textfield__label" for="admin_login"><i class="fa fa-lock"></i> ID :</label>
						</div>
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
							<input class="mdl-textfield__input" type="password" id="admin_password" name="admin_password">
							<label class="mdl-textfield__label" for="admin_password"><i class="fa fa-lock"></i> Mot de passe :</label>
						</div>
					</div>
					<div class="mdl-card__actions">
						<button type="submit" name="submit" class="mdl-button mdl-button--raised mdl-button--colored">
							OK
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
